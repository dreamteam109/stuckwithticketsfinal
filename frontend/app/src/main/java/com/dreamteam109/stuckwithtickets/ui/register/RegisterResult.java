package com.dreamteam109.stuckwithtickets.ui.register;

import androidx.annotation.Nullable;

class RegisterResult {
    private boolean isSuccessful;
    @Nullable
    private String errorMessage;

    RegisterResult() {
        this.errorMessage = null;
        this.isSuccessful = true;
    }

    RegisterResult(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
        this.isSuccessful = false;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    @Nullable
    String getErrorMessage() {
        return errorMessage;
    }
}