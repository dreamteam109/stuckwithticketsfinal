package com.dreamteam109.stuckwithtickets.model.restapi;

import android.util.Log;

import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.AccessAndRefreshTokens;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

class TokenAuthenticator implements Authenticator {
    @Override
    public synchronized Request authenticate(Route route, Response response) throws IOException {
        Log.d("StuckWithTicketsLog", "TokenAuthenticator - authenticate");
        AccessAndRefreshTokens refreshResult = ModelRestApi.instance.refreshToken();
        Log.d("StuckWithTicketsLog", "TokenAuthenticator - authenticate - refreshResult = " + refreshResult);

        if (refreshResult != null) {
            // execute failed request again with new access token
            return response.request().newBuilder()
                    .header("Authorization", "Bearer " + refreshResult.getAccessToken())
                    .build();
        } else {
            // Refresh token failed, you can logout user or retry couple of times
            // Returning null is critical here, it will stop the current request
            // If you do not return null, you will end up in a loop calling refresh
            return null;
        }
    }
}
