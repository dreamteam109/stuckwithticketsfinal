package com.dreamteam109.stuckwithtickets.ui.ticketdetails;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.databinding.FragmentTicketDetailsBinding;
import com.dreamteam109.stuckwithtickets.model.Model;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class TicketDetailsFragment extends Fragment {

    private TicketDetailsViewModel ticketDetailsViewModel;
    private FragmentTicketDetailsBinding binding;

    TextView ticketNameTv;
    ImageView imageImv;
    TextView categoryTv;
    TextView descriptionTv;
    TextView dateTv;
    TextView timeTv;
    TextView amountTv;
    TextView priceTv;
    TextView locationTv;
    ConstraintLayout buttonsOtherUsersCl;
    ConstraintLayout buttonsOwnByCurrentUserCl;
    Button contactInfoBtn;
    Button deleteBtn;
    Button editBtn;
    ProgressBar progressBar;

    int ticketId;
    Geocoder geocoder = new Geocoder(StuckWithTicketsApplication.getContext(), Locale.getDefault());

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentTicketDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ticketNameTv = root.findViewById(R.id.ticket_details_name);
        categoryTv = root.findViewById(R.id.ticket_details_category_field_tv);
        descriptionTv = root.findViewById(R.id.ticket_details_description_field_tv);
        imageImv = root.findViewById(R.id.ticket_details_image_imv);
        dateTv = root.findViewById(R.id.ticket_details_event_date_field_tv);
        timeTv = root.findViewById(R.id.ticket_details_time_field_tv);
        amountTv = root.findViewById(R.id.ticket_details_amount_field_tv);
        priceTv = root.findViewById(R.id.ticket_details_price_field_tv);
        locationTv = root.findViewById(R.id.ticket_details_location_field_tv);
        buttonsOtherUsersCl = root.findViewById(R.id.ticket_details_buttons_other_users_cl);
        buttonsOwnByCurrentUserCl = root.findViewById(R.id.ticket_details_buttons_owned_by_current_user_cl);
        contactInfoBtn = root.findViewById(R.id.ticket_details_contact_btn);
        deleteBtn = root.findViewById(R.id.ticket_details_delete_btn);
        editBtn = root.findViewById(R.id.ticket_details_edit_btn);
        progressBar = root.findViewById(R.id.ticket_details_progressbar);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ticketId = TicketDetailsFragmentArgs.fromBundle(getArguments()).getTicketId();
        ticketDetailsViewModel = new ViewModelProvider(this, new TicketDetailsViewModel.Factory(getActivity().getApplication(), ticketId))
                .get(TicketDetailsViewModel.class);
        progressBar.setVisibility(View.VISIBLE);

        ticketDetailsViewModel.getTicket().observe(getViewLifecycleOwner(), ticket -> {
            if (ticket != null) {
                if (ticket.getId() != ticketId) {
                    return;
                }

                ticketNameTv.setText(ticket.getEventName());
                Glide.with(TicketDetailsFragment.this)
                        .load(Model.instance.getFullImageUrl(ticket.getImageId()))
                        .fitCenter()
                        .placeholder(R.drawable.event_image_placeholder)
                        .fallback(R.drawable.event_image_placeholder)
                        .into(imageImv);
                categoryTv.setText(ticket.getCategoryName());
                descriptionTv.setText(ticket.getDescription());
                dateTv.setText(Model.instance.dateFormatter.format(ticket.getEventDateTime()));
                timeTv.setText(Model.instance.timeFormatter.format(ticket.getEventDateTime()));
                amountTv.setText(Integer.toString(ticket.getAmount()));
                priceTv.setText("$" + Integer.toString(ticket.getPrice()));

                contactInfoBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("StuckWithTicketsLog", "TicketDetailsFragment - Contact info button clicked");
                        Navigation.findNavController(v).navigate(TicketDetailsFragmentDirections.actionNavDetailsToTicketContactInfoFragment(ticket.getUserId()));
                    }
                });
                editBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("StuckWithTicketsLog", "TicketDetailsFragment - Ticket id " + ticketId + " clicked to edit.");
                        Navigation.findNavController(view).navigate(TicketDetailsFragmentDirections.actionNavTicketDetailsFragmentToEditTicketFragment(ticketId));
                    }
                });
                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("StuckWithTicketsLog", "TicketDetailsFragment - Ticket id " + ticketId + " clicked to delete (opening yes/no dialog).");
                        new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme)
                                .setMessage(R.string.delete_ticket_dialog)
                                .setPositiveButton(R.string.yes, (dialog, which) -> {
                                    Model.instance.deleteTicket(ticketId, () -> {
                                        Navigation.findNavController(view).navigateUp();
                                        Toast.makeText(getContext(), R.string.ticket_deleted_dialog, Toast.LENGTH_LONG).show();
                                    });
                                })
                                .setNegativeButton(R.string.no, (dialog, which) -> {
                                })
                                .show();
                    }
                });

                boolean doesUserOwnCurrentTicket = false;
                if (Model.instance.getCurrentUserId().getValue() != null) {
                    doesUserOwnCurrentTicket = Model.instance.getCurrentUserId().getValue().equals(ticket.getUserId());
                }
                displayAppropriateButtons(doesUserOwnCurrentTicket);

                Double latitude = ticket.getLocationPnt().latitude;
                Double longitude = ticket.getLocationPnt().longitude;

                List<Address> address;
                try {
                    address = geocoder.getFromLocation(latitude, longitude, 1);
                    locationTv.setText(address.get(0).getAddressLine(0));

                } catch (IOException e) {
                    e.printStackTrace();
                }

                progressBar.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        ticketDetailsViewModel.refreshTicket();
    }

    private void displayAppropriateButtons(Boolean doesUserOwnCurrentTicket) {
        if (doesUserOwnCurrentTicket) {
            buttonsOtherUsersCl.setVisibility(View.GONE);
            buttonsOwnByCurrentUserCl.setVisibility(View.VISIBLE);
        } else {
            buttonsOtherUsersCl.setVisibility(View.VISIBLE);
            buttonsOwnByCurrentUserCl.setVisibility(View.GONE);
        }
    }
}