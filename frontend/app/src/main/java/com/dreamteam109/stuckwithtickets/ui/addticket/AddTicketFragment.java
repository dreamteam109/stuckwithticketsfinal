package com.dreamteam109.stuckwithtickets.ui.addticket;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.databinding.FragmentAddTicketBinding;
import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.Validators;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class AddTicketFragment extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE = 2;

    private AddTicketViewModel addTicketViewModel;

    private FragmentAddTicketBinding binding;
    private List<Category> allCategories = new ArrayList<Category>();

    TextInputLayout titleTil;
    Spinner categorySp;
    TextInputLayout descriptionTil;
    CalendarView dateCv;
    TimePicker timeTp;
    TextInputLayout amountTil;
    TextInputLayout priceTil;
    ImageView imageImv;
    Button cameraBtn;
    Button clearImageBtn;
    Button galleryBtn;
    TextInputLayout locationTil;
    Button saveBtn;
    Button cancelBtn;
    ArrayAdapter<String> categoriesAdapter;
    ProgressBar progressBar;

    Geocoder geocoder = new Geocoder(StuckWithTicketsApplication.getContext(), Locale.getDefault());

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentAddTicketBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        addTicketViewModel = new ViewModelProvider(this).get(AddTicketViewModel.class);

        titleTil = root.findViewById(R.id.add_ticket_title_field_til);
        categorySp = root.findViewById(R.id.add_ticket_category_field_spinner);
        descriptionTil = root.findViewById(R.id.add_ticket_description_field_til);
        dateCv = root.findViewById(R.id.add_ticket_date_field_cv);
        timeTp = root.findViewById(R.id.add_ticket_time_field_tp);
        amountTil = root.findViewById(R.id.add_ticket_amount_field_til);
        priceTil = root.findViewById(R.id.add_ticket_price_field_til);
        imageImv = root.findViewById(R.id.add_ticket_image_imv);
        cameraBtn = root.findViewById(R.id.add_ticket_camera_btn);
        clearImageBtn = root.findViewById(R.id.add_ticket_clear_image_btn);
        galleryBtn = root.findViewById(R.id.add_ticket_gallery_btn);
        locationTil = root.findViewById(R.id.add_ticket_location_field_til);
        saveBtn = root.findViewById(R.id.add_ticket_save_btn);
        cancelBtn = root.findViewById(R.id.add_ticket_cancel_btn);
        progressBar = root.findViewById(R.id.add_ticket_progressbar);

        long tomorrow = new Date().getTime() + 1000L * 60 * 60 * 24;
        dateCv.setDate(tomorrow);
        addTicketViewModel.setDate(tomorrow);
        addTicketViewModel.getCategoriesList().observe(getViewLifecycleOwner(), new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categories) {
                progressBar.setVisibility(View.VISIBLE);

                allCategories = categories;

                Log.d("TAG", categories.toString());
                categoriesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                        allCategories.stream().map(category -> category.getName()).collect(Collectors.toList()));
                categorySp.setAdapter(categoriesAdapter);
                progressBar.setVisibility(View.GONE);


            }
        });
        categoriesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                allCategories.stream().map(category -> category.getName()).collect(Collectors.toList()));
        categorySp.setAdapter(categoriesAdapter);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);

                Log.d("StuckWithTicketsLog", "sell ticket save click");
                if (!Validators.isNotEmptyString(addTicketViewModel.getTitle().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.title_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotEmptyString(addTicketViewModel.getDescription().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.description_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotNull(addTicketViewModel.getAmount().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.amount_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotNull(addTicketViewModel.getPrice().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.price_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotEmptyString(addTicketViewModel.getLocation().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.location_is_empty, Toast.LENGTH_LONG).show();
                } else {
                    try {
                        List<Address> listAdress = geocoder.getFromLocationName(addTicketViewModel.getLocation().getValue(), 1);
                        if (listAdress.size() == 0) {
                            Toast.makeText(getContext().getApplicationContext(), R.string.location_is_invalid, Toast.LENGTH_LONG).show();
                        } else {
                            Address addLocation = listAdress.get(0);
                            addTicketViewModel.sellTicket(timeTp.getHour(), timeTp.getMinute(), addLocation.getLatitude(),
                                    addLocation.getLongitude(), new Model.SellTicketListener() {
                                        @Override
                                        public void onComplete() {
                                            progressBar.setVisibility(View.GONE);
                                            Navigation.findNavController(v).navigateUp();
                                        }
                                    });
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    
                }

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);

                Log.d("TAG", "sell ticket cancel click");
                Navigation.findNavController(v).navigateUp();
            }
        });

        titleTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addTicketViewModel.setTitle(titleTil.getEditText().getText().toString());
            }
        }));

        categorySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(" TAG", categorySp.getSelectedItem().toString());
                addTicketViewModel.setCategory(allCategories.stream().
                        filter(category -> category.getName().
                                equals(categorySp.getSelectedItem().toString())).collect(Collectors.toList()).get(0).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(" TAG", "category is not clicked");

            }
        });

        descriptionTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addTicketViewModel.setDescription(descriptionTil.getEditText().getText().toString());
            }
        }));

        dateCv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                long timestamp = LocalDate.of(year, month + 1, dayOfMonth).atTime(LocalTime.MIN).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                addTicketViewModel.setDate(timestamp);
            }
        });

        amountTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    addTicketViewModel.setAmount(Integer.parseInt(amountTil.getEditText().getText().toString()));
                } catch (Exception e) {
                    addTicketViewModel.setAmount(null);
                }
            }
        }));
        priceTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    addTicketViewModel.setPrice(Integer.parseInt(priceTil.getEditText().getText().toString()));
                } catch (Exception e) {
                    addTicketViewModel.setPrice(null);
                }
            }
        }));

        addTicketViewModel.getBitmap().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                if (bitmap == null) {
                    imageImv.setImageResource(R.drawable.event_image_placeholder);
                } else {
                    imageImv.setImageBitmap(bitmap);
                }
            }
        });

        cameraBtn.setOnClickListener(v -> {
            openSelectImageFromCamera();
        });

        clearImageBtn.setOnClickListener(v -> {
            clearImage();
        });

        galleryBtn.setOnClickListener(v -> {
            openSelectImageFromGallery();
        });

        locationTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                addTicketViewModel.setLocation(locationTil.getEditText().getText().toString());
            }
        }));
        progressBar.setVisibility(View.GONE);

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        progressBar.setVisibility(View.VISIBLE);

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                addTicketViewModel.setBitmap((Bitmap) extras.get("data"));
            }
        } else if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(StuckWithTicketsApplication.getContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    Toast.makeText(getContext(), R.string.problem_getting_selected_image, Toast.LENGTH_LONG);
                }
                addTicketViewModel.setBitmap(bitmap);
            }
        }
        progressBar.setVisibility(View.GONE);

    }

    private void openSelectImageFromCamera() {
        progressBar.setVisibility(View.VISIBLE);

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
        progressBar.setVisibility(View.GONE);

    }

    private void openSelectImageFromGallery() {
        progressBar.setVisibility(View.VISIBLE);

        Intent intent = new Intent();
        intent.setType("image/png image/jpeg");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        progressBar.setVisibility(View.GONE);

    }

    private void clearImage() {
        addTicketViewModel.setBitmap(null);
    }
}