package com.dreamteam109.stuckwithtickets.ui.profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.User;

public class ProfileViewModel extends ViewModel {

    public LiveData<User> getProfileInfo() {
        return Model.instance.getProfileInfo();
    }

}