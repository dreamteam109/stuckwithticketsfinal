package com.dreamteam109.stuckwithtickets.ui.map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.databinding.FragmentMapBinding;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;

public class MapFragment extends Fragment {

    ProgressBar progressBar;
    private FragmentMapBinding binding;
    MapViewModel mapViewModel;
    View root;
    MarkerOptions markerStart;
    SupportMapFragment supportMapFragment;
    Location currentLocation = new Location("");
    Marker flag;
    GoogleMap googleMap;

    FusedLocationProviderClient fusedLocationProviderClient;

    private ActivityResultLauncher<String> requestPermissionResult = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            new ActivityResultCallback<Boolean>() {
                @Override
                public void onActivityResult(Boolean result) {
                    if (result) {
                        Log.d("StuckWithTicketsLog", "onActivityResult: permission granted");
                        fetchLastLocation();
                    } else {
                        Log.e("StuckWithTicketsLog", "onActivityResult: PERMISSION DENIED");
                    }
                }
            });

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMapBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        progressBar = root.findViewById(R.id.map_progressbar);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mapViewModel = new ViewModelProvider(this).get(MapViewModel.class);
        supportMapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.google_map_layout);

        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull GoogleMap googleMap) {
                MapFragment.this.googleMap = googleMap;

                Geocoder geocoder = new Geocoder(getContext());
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocationName("Israel", 1);
                    if (addresses.size() > 0) {
                        double latitude = addresses.get(0).getLatitude();
                        double longitude = addresses.get(0).getLongitude();
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 7.5f));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        updatePoints(googleMap);
                    }
                }, 1000);

                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(@NonNull Marker marker) {
                        try {
                            Ticket ticket = (Ticket) marker.getTag();
                            Navigation.findNavController(root).navigate(MapFragmentDirections.actionMapFragmentToNavTicketDetailsFragment(ticket.getId()));
                        } catch (Exception exception) {

                        }
                        return true;
                    }
                });

                fetchLastLocation();
            }
        });

        return root;
    }

    private void fetchLastLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissionResult.launch(Manifest.permission.ACCESS_FINE_LOCATION);
            return;
        }

        Task<Location> task = fusedLocationProviderClient.getCurrentLocation(100, null);

        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation.set(location);

                    Log.d("StuckWithTicketsLog", "MapFragment - fetchLastLocation - onSuccess - "
                            + currentLocation.getLatitude() + ", " + currentLocation.getLongitude());
                    LatLng latLngStart = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngStart, 10));
                    markerStart = new MarkerOptions().position(latLngStart);
                    flag = googleMap.addMarker(markerStart);
                    flag.setIcon(BitmapFromVector(StuckWithTicketsApplication.getContext(), R.drawable.ic_baseline_flag_24));
                }

            }
        });
    }

    public void updatePoints(GoogleMap googleMap) {
        progressBar.setVisibility(View.VISIBLE);
        List<Ticket> tickets = mapViewModel.getTicketsList().getValue();
        for (Ticket ticket : tickets) {
            LatLng point = ticket.getLocationPnt();
            MarkerOptions markerOptions = new MarkerOptions().position(point).title(ticket.getEventName());
            Marker marker = googleMap.addMarker(markerOptions);
            marker.setTag(ticket);
        }
        progressBar.setVisibility(View.GONE);
    }

    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);

        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}