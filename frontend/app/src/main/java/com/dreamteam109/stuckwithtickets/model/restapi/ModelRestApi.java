package com.dreamteam109.stuckwithtickets.model.restapi;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.AddImageResponseBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.EditProfileRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.EditTicketRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.LoginRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.AccessAndRefreshTokens;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.RegisterRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.SearchTicketsRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.SellTicketRequestBody;
import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;
import com.dreamteam109.stuckwithtickets.model.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

import io.jsonwebtoken.io.Decoders;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ModelRestApi {
    public static final ModelRestApi instance = new ModelRestApi();
    private static final String PREFERENCES_FILE_NAME = "ModelRestApiPreferences";
    private static final String PREFERENCES_ACCESS_TOKEN_KEY = "UserAccessToken";
    private static final String PREFERENCES_REFRESH_TOKEN_KEY = "UserRefreshToken";
    private final MutableLiveData<String> accessToken = new MutableLiveData<>();
    private final MutableLiveData<String> refreshToken = new MutableLiveData<>();
    private final LiveData<Boolean> isLoggedIn = Transformations.map(accessToken, Objects::nonNull);
    private final LiveData<JSONObject> decodedAccessToken = Transformations.map(accessToken, ModelRestApi::mapStringTokenToDecodedJson);
    private final LiveData<Integer> currentUserId = Transformations.map(decodedAccessToken, ModelRestApi::mapDecodedTokenToUserId);
    private final LiveData<String> currentUserEmail = Transformations.map(decodedAccessToken, ModelRestApi::mapDecodedTokenToUserEmail);

    private static JSONObject mapStringTokenToDecodedJson(String token) {
        JSONObject decodedJsonWebToken = null;

        if (token == null) {
            return null;
        }

        byte[] bytes = Decoders.BASE64URL.decode(token.split("\\.")[1]);
        String decoded = new String(bytes, StandardCharsets.UTF_8);
        try {
            decodedJsonWebToken = new JSONObject(decoded);
            Log.d("StuckWithTicketsLog", "ModelRestApi - decodedJsonWebToken = " + decodedJsonWebToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return decodedJsonWebToken;
    }

    private static Integer mapDecodedTokenToUserId(JSONObject decodedToken) {
        if (decodedToken == null) {
            return null;
        }

        try {
            return decodedToken.getJSONObject("user").getInt("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String mapDecodedTokenToUserEmail(JSONObject decodedToken) {
        if (decodedToken == null) {
            return null;
        }

        try {
            return decodedToken.getJSONObject("user").getString("email");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private ModelRestApi() {
        Log.d("StuckWithTicketsLog", "ModelRestApi - Constructor");
        SharedPreferences sharedPreferences = StuckWithTicketsApplication.getContext()
                .getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        this.accessToken.setValue(sharedPreferences.getString(PREFERENCES_ACCESS_TOKEN_KEY, null));
        this.refreshToken.setValue(sharedPreferences.getString(PREFERENCES_REFRESH_TOKEN_KEY, null));
    }

    public String getFullImageUrl(String imageId) {
        if (imageId == null) {
            return null;
        }

        return MyRetrofit.BASE_URL + "images/" + imageId;
    }

    String getAccessTokenHeader() {
        return "Bearer " + accessToken.getValue();
    }

    private void setAccessToken(String accessToken, boolean backgroundThread) {
        if (backgroundThread) {
            this.accessToken.postValue(accessToken);
        } else {
            this.accessToken.setValue(accessToken);
        }
        StuckWithTicketsApplication.getContext()
                .getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
                .edit()
                .putString(PREFERENCES_ACCESS_TOKEN_KEY, accessToken)
                .commit();
    }

    String getRefreshTokenHeader() {
        return "Bearer " + refreshToken.getValue();
    }

    private void setRefreshToken(String refreshToken, boolean backgroundThread) {
        if (backgroundThread) {
            this.refreshToken.postValue(refreshToken);
        } else {
            this.refreshToken.setValue(refreshToken);
        }
        StuckWithTicketsApplication.getContext()
                .getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE)
                .edit()
                .putString(PREFERENCES_REFRESH_TOKEN_KEY, refreshToken)
                .commit();
    }

    public LiveData<Boolean> isLoggedIn() {
        return this.isLoggedIn;
    }

    public LiveData<Integer> getCurrentUserId() {
        return this.currentUserId;
    }

    public LiveData<String> getCurrentUserEmail() {
        return this.currentUserEmail;
    }


    public interface GetAllCategoriesListener {
        void onComplete(List<Category> list);

        void onError(String message);
    }

    public void getAllCategories(GetAllCategoriesListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - getAllCategories");
        Call<List<Category>> call = MyRetrofit.getInstance().getCategoriesApi().getCategories();
        call.enqueue(new Callback<List<Category>>() {

            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getAllCategories - onResponse - successful");
                    List<Category> categories;
                    categories = response.body();
                    listener.onComplete(categories);
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getAllCategories - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - getAllCategories - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface SearchTicketsListener {
        void onComplete(List<Ticket> list);

        void onError(String message);
    }

    public void searchTickets(String text, Integer priceLimit, Integer amount, List<Integer> categories, Long startTimestamp, Long endTimestamp, Long lastUpdateTimestamp, SearchTicketsListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - searchTickets");
        Call<List<Ticket>> call = MyRetrofit
                .getInstance()
                .getTicketsApi()
                .searchTickets(
                        isLoggedIn().getValue() != null && isLoggedIn().getValue() ? getAccessTokenHeader() : null,
                        new SearchTicketsRequestBody(
                                lastUpdateTimestamp,
                                text,
                                categories,
                                startTimestamp,
                                endTimestamp,
                                priceLimit,
                                amount
                        )
                );
        call.enqueue(new Callback<List<Ticket>>() {

            @Override
            public void onResponse(Call<List<Ticket>> call, Response<List<Ticket>> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - searchTickets - onResponse - successful");
                    List<Ticket> tickets;
                    tickets = response.body();
                    listener.onComplete(tickets);
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - searchTickets - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Ticket>> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - searchTickets - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface GetContactInfoByIdListener {
        void onComplete(User contactInfo);

        void onError(String message);
    }

    public void getContactInfoById(int userId, GetContactInfoByIdListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - getContactInfoById");
        Call<User> call = MyRetrofit.getInstance().getUsersApi().getContactInfoById(userId);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getContactInfoById - onResponse - successful");
                    User contactInfo;
                    contactInfo = response.body();
                    listener.onComplete(contactInfo);
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getContactInfoById - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - getContactInfoById - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface GetProfileInfoListener {
        void onComplete(User profileInfo);

        void onError(String message);
    }

    public void getProfileInfo(GetProfileInfoListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - getProfileInfo");
        Call<User> call = MyRetrofit.getInstance().getUsersApi().getProfileInfo(getAccessTokenHeader());

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getProfileInfo - onResponse - successful");
                    User profileInfo;
                    profileInfo = response.body();
                    listener.onComplete(profileInfo);
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getProfileInfo - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - getProfileInfo - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface RegisterListener {
        void onComplete();

        void onError(String message);
    }

    public void register(RegisterRequestBody registerRequestBody, RegisterListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - register");
        Call<Void> call = MyRetrofit.getInstance().getUsersApi().register(registerRequestBody);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - register - onResponse - successful");
                    listener.onComplete();
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - register - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - register - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface LoginListener {
        void onComplete();

        void onError(String message);
    }

    public void login(LoginRequestBody loginRequestBody, LoginListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - login");
        Call<AccessAndRefreshTokens> call = MyRetrofit.getInstance().getUsersApi().login(loginRequestBody);
        call.enqueue(new Callback<AccessAndRefreshTokens>() {
            @Override
            public void onResponse(Call<AccessAndRefreshTokens> call, Response<AccessAndRefreshTokens> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - login - onResponse - successful");
                    AccessAndRefreshTokens loginResponseBody = response.body();
                    setAccessToken(loginResponseBody.getAccessToken(), false);
                    setRefreshToken(loginResponseBody.getRefreshToken(), false);
                    listener.onComplete();
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - login - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AccessAndRefreshTokens> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - login - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public AccessAndRefreshTokens refreshToken() {
        Log.d("StuckWithTicketsLog", "ModelRestApi - refreshToken");
        Call<AccessAndRefreshTokens> call = MyRetrofit.getInstance().getTokenRefreshApi().refreshToken(getRefreshTokenHeader());
        try {
            Response<AccessAndRefreshTokens> response = call.execute();
            if (response.isSuccessful()) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - refreshToken - successful");
                AccessAndRefreshTokens refreshTokenResponseBody = response.body();
                setAccessToken(refreshTokenResponseBody.getAccessToken(), true);
                setRefreshToken(refreshTokenResponseBody.getRefreshToken(), true);
                return refreshTokenResponseBody;
            } else {
                Log.d("StuckWithTicketsLog", "ModelRestApi - refreshToken - not successful");
                setAccessToken(null, true);
                setRefreshToken(null, true);
                return null;
            }
        } catch (IOException e) {
            Log.d("StuckWithTicketsLog", "ModelRestApi - refreshToken - catch block");
            return null;
        }
    }

    public interface LogoutListener {
        void onAfter();
    }

    public void logout(LogoutListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - logout");
        Call<Void> call = MyRetrofit.getInstance().getUsersApi().logout(getRefreshTokenHeader());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - logout - onResponse - isSuccessful = " + response.isSuccessful());
                setAccessToken(null, false);
                setRefreshToken(null, false);
                listener.onAfter();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - logout - onFailure");
                setAccessToken(null, false);
                setRefreshToken(null, false);
                listener.onAfter();
            }
        });


    }

    private File bitmapToFile(Bitmap imageBitmap) {
        File file = new File(StuckWithTicketsApplication.getContext().getCacheDir(), "imageFile");

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Bitmap bitmap = imageBitmap;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] bitmapData = baos.toByteArray();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(bitmapData);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }

    public interface AddImageListener {
        void onComplete(String imageId);

        void onError(String message);
    }

    public void addImage(Bitmap imageBitmap, AddImageListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - addImage");

        File file = bitmapToFile(imageBitmap);
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/jpeg"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        Call<AddImageResponseBody> call = MyRetrofit.getInstance().getImagesApi()
                .addImage(body);

        call.enqueue(new Callback<AddImageResponseBody>() {
            @Override
            public void onResponse(Call<AddImageResponseBody> call, Response<AddImageResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - addImage - onResponse - successful");
                    String imageId = response.body().getImageId();
                    listener.onComplete(imageId);
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - addImage - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddImageResponseBody> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - addImage - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface SellTicketListener {
        void onComplete();

        void onError(String message);
    }

    public void sellTicket(String title, int category, String description, Long eventTimestamp, int amount, int price, String imageId, double x, double y, SellTicketListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - sellTicket");
        Call<Void> call = MyRetrofit
                .getInstance()
                .getTicketsApi()
                .sellTicket(getAccessTokenHeader(),
                        new SellTicketRequestBody(
                                title,
                                category,
                                description,
                                eventTimestamp,
                                amount,
                                price,
                                imageId,
                                x,
                                y
                        )
                );
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - sellTicket - onResponse - successful");
                    listener.onComplete();
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - sellTicket - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - sellTicket - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface GetMyTicketsListener {
        void onComplete(List<Ticket> list);

        void onError(String message);
    }

    public void getMyTickets(GetMyTicketsListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - getMyTickets");
        Call<List<Ticket>> call = MyRetrofit.getInstance().getTicketsApi().getMyTickets(getAccessTokenHeader());
        call.enqueue(new Callback<List<Ticket>>() {
            @Override
            public void onResponse(Call<List<Ticket>> call, Response<List<Ticket>> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getMyTickets - onResponse - successful");
                    List<Ticket> tickets;
                    tickets = response.body();
                    listener.onComplete(tickets);
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getMyTickets - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Ticket>> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - getMyTickets - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface GetTicketDetailsListener {
        void onComplete(Ticket ticket);

        void onError(String message);
    }

    public void getTicketDetails(int id, GetTicketDetailsListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - getTicketDetails");
        Call<Ticket> call = MyRetrofit.getInstance().getTicketsApi().getTicketById(id);
        call.enqueue(new Callback<Ticket>() {
            @Override
            public void onResponse(Call<Ticket> call, Response<Ticket> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getTicketDetails - onResponse - successful");
                    Ticket ticket;
                    ticket = response.body();
                    listener.onComplete(ticket);
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - getTicketDetails - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Ticket> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - getTicketDetails - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface EditTicketListener {
        void onComplete();

        void onError(String message);
    }

    public void editTicket(int id, String title, int category, String description, Long eventTimestamp, int amount, int price, String imageId, double x, double y, EditTicketListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - editTicket");
        Call<Void> call = MyRetrofit.getInstance().getTicketsApi().editTicket(
                getAccessTokenHeader(), id, new EditTicketRequestBody(
                        title,
                        category,
                        description,
                        eventTimestamp,
                        amount,
                        price,
                        imageId,
                        x,
                        y
                )
        );
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - editTicket - onResponse - successful");
                    listener.onComplete();
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - editTicket - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - editTicket - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface DeleteTicketListener {
        void onComplete();

        void onError(String message);
    }

    public void deleteTicket(int id, DeleteTicketListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - deleteTicket");
        Call<Void> call = MyRetrofit.getInstance().getTicketsApi().deleteTicket(
                getAccessTokenHeader(),
                id
        );

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - deleteTicket - onResponse - successful");
                    listener.onComplete();
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - deleteTicket - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - deleteTicket - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }

    public interface EditProfileListener {
        void onComplete();

        void onError(String message);
    }

    public void editProfile(int id, String firstName, String lastName, String phoneNumber, String imageId,String interests, EditProfileListener listener) {
        Log.d("StuckWithTicketsLog", "ModelRestApi - editProfile");
        Call<Void> call = MyRetrofit.getInstance().getUsersApi().editProfile(
                getAccessTokenHeader(), new EditProfileRequestBody(
                        firstName,
                        lastName,
                        phoneNumber,
                        imageId,
                        interests
                )
        );
        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - editProfile - onResponse - successful");
                    listener.onComplete();
                } else {
                    Log.d("StuckWithTicketsLog", "ModelRestApi - editProfile - onResponse - not successful");
                    try {
                        listener.onError(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        listener.onError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("StuckWithTicketsLog", "ModelRestApi - editProfile - onFailure");
                listener.onError(t.getMessage());
            }
        });
    }
}
