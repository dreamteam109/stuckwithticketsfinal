package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

import androidx.annotation.Nullable;

public class RegisterRequestBody {
    final String firstName;
    final String lastName;
    final String email;
    final String password;
    final String phoneNumber;
    final String imageId;
    final String interests;

    public RegisterRequestBody(String firstName, String lastName, String email, String password, String phoneNumber, @Nullable String imageId, String interests) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.imageId = imageId;
        this.interests = interests;
    }
}
