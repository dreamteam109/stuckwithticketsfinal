package com.dreamteam109.stuckwithtickets.ui.editticket;

import android.app.Application;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EditTicketViewModel extends AndroidViewModel {
    private LiveData<Ticket> ticket;
    private MutableLiveData<String> title = new MutableLiveData<String>();
    private MutableLiveData<Integer> category = new MutableLiveData<Integer>();
    private MutableLiveData<String> description = new MutableLiveData<String>();
    private MutableLiveData<Long> date = new MutableLiveData<Long>();
    private MutableLiveData<Integer> amount = new MutableLiveData<Integer>();
    private MutableLiveData<Integer> price = new MutableLiveData<Integer>();
    private MutableLiveData<Bitmap> bitmap = new MutableLiveData<Bitmap>();
    private MutableLiveData<String> location = new MutableLiveData<String>();

    private boolean bitmapWasSet = false;

    public EditTicketViewModel(@NonNull Application application, int ticketId) {
        super(application);
        ticket = Model.instance.getTicketById();
        Model.instance.refreshTicketById(ticketId);
    }

    public MutableLiveData<String> getTitle() {
        return title;
    }

    public MutableLiveData<Integer> getCategory() {
        return category;
    }

    public MutableLiveData<String> getDescription() {
        return description;
    }

    public MutableLiveData<Long> getDate() {
        return date;
    }

    public MutableLiveData<Integer> getAmount() {
        return amount;
    }

    public MutableLiveData<Integer> getPrice() {
        return price;
    }

    public MutableLiveData<Bitmap> getBitmap() {
        return bitmap;
    }

    public MutableLiveData<String> getLocation() {
        return location;
    }

    public LiveData<List<Category>> getCategoriesList() {
        return Model.instance.getAllCategories();
    }

    public LiveData<Ticket> getTicket() {
        return ticket;
    }

    public void setDate(Long date) {
        this.date.setValue(date);
    }

    public void setTitle(String title) {
        this.title.setValue(title);
    }

    public void setCategory(Integer category) {
        this.category.setValue(category);
    }

    public void setDescription(String description) {
        this.description.setValue(description);
    }

    public void setAmount(Integer amount) {
        this.amount.setValue(amount);
    }

    public void setPrice(Integer price) {
        this.price.setValue(price);
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmapWasSet = true;
        this.bitmap.setValue(bitmap);
    }

    public void setLocation(String location) {
        this.location.setValue(location);
    }

    public void editTicket(int hour, int minute, double x, double y, Model.EditTicketListener listener) {
        Date date = new Date(this.date.getValue());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        if (!bitmapWasSet) {
            Model.instance.editTicket(
                    ticket.getValue().getId(),
                    title.getValue(),
                    category.getValue(),
                    description.getValue(),
                    calendar.getTimeInMillis(),
                    amount.getValue(),
                    price.getValue(),
                    getTicket().getValue().getImageId(),
                    x,
                    y,
                    listener);
        } else {
            if (bitmap.getValue() == null) {
                Model.instance.editTicket(
                        ticket.getValue().getId(),
                        title.getValue(),
                        category.getValue(),
                        description.getValue(),
                        calendar.getTimeInMillis(),
                        amount.getValue(),
                        price.getValue(),
                        null,
                        x,
                        y,
                        listener);
            } else {
                Model.instance.addImage(bitmap.getValue(), new Model.AddImageListener() {
                    @Override
                    public void onComplete(String imageId) {
                        Model.instance.editTicket(
                                ticket.getValue().getId(),
                                title.getValue(),
                                category.getValue(),
                                description.getValue(),
                                calendar.getTimeInMillis(),
                                amount.getValue(),
                                price.getValue(),
                                imageId,
                                x,
                                y,
                                listener);
                    }
                });
            }
        }
    }


    public static class Factory implements ViewModelProvider.Factory {

        @NonNull
        private final Application application;

        private final int ticketId;

        public Factory(@NonNull Application application, int ticketId) {
            this.application = application;
            this.ticketId = ticketId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> aClass) {
            return (T) new EditTicketViewModel(application, ticketId);
        }
    }

}