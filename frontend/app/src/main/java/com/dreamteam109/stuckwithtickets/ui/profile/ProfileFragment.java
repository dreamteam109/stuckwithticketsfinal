package com.dreamteam109.stuckwithtickets.ui.profile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.databinding.FragmentProfileBinding;
import com.dreamteam109.stuckwithtickets.model.Model;

public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    private FragmentProfileBinding binding;
    ProgressBar progressBar;
    ImageView userImv;
    TextView firstNameTv;
    TextView lastNameTv;
    TextView phoneTv;
    TextView emailTv;
    TextView interestsTv;
    Button editBtn;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        firstNameTv = root.findViewById(R.id.profile_first_name_field_tv);
        lastNameTv = root.findViewById(R.id.profile_last_name_field_tv);
        phoneTv = root.findViewById(R.id.profile_phone_field_tv);
        emailTv = root.findViewById(R.id.profile_email_field_tv);
        interestsTv = root.findViewById(R.id.profile_interests_field_tv);
        userImv = root.findViewById(R.id.profile_image_imv);
        editBtn = root.findViewById(R.id.profile_edit_btn);
        progressBar = root.findViewById(R.id.profile_progressbar);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        progressBar.setVisibility(View.VISIBLE);

        profileViewModel.getProfileInfo().observe(getViewLifecycleOwner(), profileInfo -> {
            editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    progressBar.setVisibility(View.VISIBLE);
                    Log.d("StuckWithTicketsLog", "ProfileFragment - Edit profile button clicked");
                    Navigation.findNavController(v).navigate(ProfileFragmentDirections.actionProfileFragmentToEditProfileFragment());
                }
            });

            if (profileInfo != null) {
                firstNameTv.setText(profileInfo.getFirstName());
                lastNameTv.setText(profileInfo.getLastName());
                phoneTv.setText(profileInfo.getPhoneNumber());
                emailTv.setText(profileInfo.getEmail());
                interestsTv.setText(profileInfo.getInterests());
                String imageId = profileInfo.getImageId();

                Glide.with(ProfileFragment.this)
                        .load(Model.instance.getFullImageUrl(imageId))
                        .fitCenter()
                        .placeholder(R.drawable.avatar)
                        .fallback(R.drawable.avatar)
                        .into(userImv);

                progressBar.setVisibility(View.GONE);
            }
        });

    }
}