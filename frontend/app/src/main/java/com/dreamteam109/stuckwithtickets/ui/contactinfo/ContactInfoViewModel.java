package com.dreamteam109.stuckwithtickets.ui.contactinfo;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.User;

public class ContactInfoViewModel extends AndroidViewModel {
    private LiveData<User> contactInfo;

    public ContactInfoViewModel(@NonNull Application application, int userId) {
        super(application);
        contactInfo = Model.instance.getContactInfoById(userId);
    }

    public LiveData<User> getContactInfo() {
        return contactInfo;
    }

    public static class Factory implements ViewModelProvider.Factory {

        @NonNull
        private final Application application;

        private final int userId;

        public Factory(@NonNull Application application, int userId) {
            this.application = application;
            this.userId = userId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> aClass) {
            return (T) new ContactInfoViewModel(application, userId);
        }
    }

}