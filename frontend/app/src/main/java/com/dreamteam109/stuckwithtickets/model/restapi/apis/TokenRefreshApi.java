package com.dreamteam109.stuckwithtickets.model.restapi.apis;

import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.AccessAndRefreshTokens;

import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface TokenRefreshApi {

    @POST("users/refreshToken")
    Call<AccessAndRefreshTokens> refreshToken(@Header("Authorization") String header);
}
