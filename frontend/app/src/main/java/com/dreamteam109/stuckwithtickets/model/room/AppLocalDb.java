package com.dreamteam109.stuckwithtickets.model.room;

import androidx.room.Room;

import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;

public class AppLocalDb {
    static public AppLocalDbRepository db =
            Room.databaseBuilder(StuckWithTicketsApplication.getContext(),
                    AppLocalDbRepository.class,
                    "dbFileName.db")
                    .fallbackToDestructiveMigration()
                    .build();
}

