package com.dreamteam109.stuckwithtickets.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.core.os.HandlerCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.model.room.AppLocalDb;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.LoginRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.RegisterRequestBody;
import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;
import com.dreamteam109.stuckwithtickets.model.models.User;
import com.dreamteam109.stuckwithtickets.model.restapi.ModelRestApi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Model {
    public static final Model instance = new Model();
    public final SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    public final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
    public final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
    ModelRestApi modelRestApi = ModelRestApi.instance;
    public Executor executor = Executors.newFixedThreadPool(1);
    MutableLiveData<List<Category>> categoriesList = new MutableLiveData<List<Category>>();
    MutableLiveData<List<Ticket>> ticketsList = new MutableLiveData<List<Ticket>>();
    MutableLiveData<List<Ticket>> myTicketsList = new MutableLiveData<List<Ticket>>();
    MutableLiveData<Ticket> ticketById = new MutableLiveData<Ticket>();
    MutableLiveData<User> contactInfoById = new MutableLiveData<User>();
    MutableLiveData<User> profileInfo = new MutableLiveData<User>();
    public Handler mainThread = HandlerCompat.createAsync(Looper.getMainLooper());


    public enum TicketListLoadingState {
        loading,
        loaded
    }

    long now = new Date().getTime();

    MutableLiveData<TicketListLoadingState> ticketListLoadingState = new MutableLiveData<TicketListLoadingState>();
    MutableLiveData<TicketListLoadingState> myTicketListLoadingState = new MutableLiveData<TicketListLoadingState>();
    MutableLiveData<String> searchString = new MutableLiveData<String>();
    MutableLiveData<Integer> priceLimit = new MutableLiveData<Integer>();
    MutableLiveData<Integer> amount = new MutableLiveData<Integer>();
    MutableLiveData<Long> startTimestamp = new MutableLiveData<Long>(now);
    MutableLiveData<Long> endTimestamp = new MutableLiveData<Long>(now + 1000L * 60 * 60 * 24 * 365);
    MutableLiveData<List<Integer>> categories = new MutableLiveData<List<Integer>>();

    private Model() {
        Log.d("StuckWithTicketsLog", "Model - Constructor");
    }

    public LiveData<TicketListLoadingState> getTicketListLoadingState() {
        return ticketListLoadingState;
    }

    public LiveData<TicketListLoadingState> getMyTicketListLoadingState() {
        return myTicketListLoadingState;
    }

    public String getFullImageUrl(String imageId) {
        return modelRestApi.getFullImageUrl(imageId);
    }

    public void setSelectedCategories(List<Integer> categories) {
        this.categories.setValue(categories);
    }

    public List<String> getSelectedCategories() {
        return Optional.ofNullable(this.categories.getValue()).orElse(Collections.emptyList()).stream().map(this::getCategoryById).map(category -> category.name).collect(Collectors.toList());
    }

    public void setStartTimestamp(Long startTimestamp) {
        this.startTimestamp.postValue(startTimestamp);
    }

    public Long getStartTimestamp() {
        return this.startTimestamp.getValue();
    }

    public void setEndTimestamp(Long endTimestamp) {
        this.endTimestamp.postValue(endTimestamp);
    }

    public Long getEndTimeStamp() {
        return this.endTimestamp.getValue();
    }

    public void setSearchString(String searchString) {
        this.searchString.setValue(searchString);
        refreshTicketsList();
    }

    public void setPriceLimit(Integer priceLimit) {
        this.priceLimit.setValue(priceLimit);
    }

    public Integer getPriceLimit() {
        return priceLimit.getValue();
    }

    public void setAmount(Integer amount) {
        this.amount.setValue(amount);
    }

    public Integer getAmount() {
        return amount.getValue();
    }


    public LiveData<List<Ticket>> getAllTickets() {

        Log.d("StuckWithTicketsLog", "Model - getAllTickets");
        if (ticketsList.getValue() == null) {
            refreshTicketsList();
        }

        return ticketsList;
    }

    public void refreshTicketsList() {
        Log.d("StuckWithTicketsLog", "Model - refreshTicketsList");

        ticketListLoadingState.setValue(TicketListLoadingState.loading);

        String text = Optional.ofNullable(searchString.getValue()).orElse("");

        Integer maxPrice = Optional.ofNullable(priceLimit.getValue()).orElse(1000000);

        Integer minAmount = Optional.ofNullable(amount.getValue()).orElse(0);

        List<Integer> categoryIds = null;

        if (Optional.ofNullable(categories.getValue()).isPresent()) {
            categoryIds = categories.getValue();

             if (categoryIds.isEmpty()) {
                 categoryIds = null;
             }
        }

        final List<Integer> finalCategoryIds = categoryIds;

        Long fromTimestamp = Optional.ofNullable(startTimestamp.getValue()).orElse(0L);

        Long toTimestamp = Optional.ofNullable(endTimestamp.getValue()).orElse(new Date().getTime() + 1000L * 60 * 60 * 24 * 365);

        // get last local update date
        Long lastUpdateTimestamp = StuckWithTicketsApplication
                .getContext()
                .getSharedPreferences("TAG", Context.MODE_PRIVATE)
                .getLong("TicketsLastUpdateTimestamp", 0);

        executor.execute(() -> {
            List<Ticket> tickets = AppLocalDb.db.ticketDao().getAll();
            Log.d("StuckWithTicketsLog", "Model - refreshTicketsList - Tickets from room: " + tickets.size());
            ticketsList.postValue(tickets);
        });

        modelRestApi.searchTickets(text, maxPrice, minAmount, finalCategoryIds, fromTimestamp, toTimestamp, lastUpdateTimestamp, new ModelRestApi.SearchTicketsListener() {
            @Override
            public void onComplete(List<Ticket> list) {
                Log.d("StuckWithTicketsLog", "Model - refreshTicketsList - searchTickets - onComplete");
                executor.execute(() -> {
                    if (list == null) {
                        Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.something_went_wrong_loading_tickets, Toast.LENGTH_LONG).show();
                    } else {
                        Long newLastUpdateTimestamp = lastUpdateTimestamp;

                        for (Ticket ticket : list) {
                            AppLocalDb.db.ticketDao().insertAll(ticket);
                            if (newLastUpdateTimestamp < ticket.getLastUpdateDate().getTime()) {
                                newLastUpdateTimestamp = ticket.getLastUpdateDate().getTime();
                            }
                        }
                        // update last local update timestamp
                        StuckWithTicketsApplication.getContext()
                                .getSharedPreferences("TAG", Context.MODE_PRIVATE)
                                .edit()
                                .putLong("TicketsLastUpdateTimestamp", newLastUpdateTimestamp)
                                .commit();

                        //return all data to caller
                        List<Ticket> tickets = AppLocalDb.db.ticketDao().getAll()
                                .stream()
                                .filter(ticket ->
                                        (ticket.getEventName().contains(text)
                                                || ticket.getDescription().contains(text))
                                                && ticket.getPrice() <= maxPrice
                                                && ticket.getAmount() >= minAmount
                                                && ticket.getEventDateTime().getTime() >= fromTimestamp
                                                && ticket.getEventDateTime().getTime() <= toTimestamp
                                                && (!Optional.ofNullable(finalCategoryIds).isPresent()
                                                || finalCategoryIds.contains(ticket.getCategoryId()))
                                )
                                .sorted((t1, t2) -> {
                                    boolean name1 = t1.getEventName().contains(text);
                                    boolean name2 = t2.getEventName().contains(text);

                                    if (name1 && !name2) {
                                        return -1;
                                    } else if (name2 && !name1) {
                                        return 1;
                                    }

                                    double score1 = t1.getScore();
                                    double score2 = t2.getScore();

                                    Calendar date1 = new GregorianCalendar();
                                    date1.setTime(t1.getEventDateTime());
                                    Calendar date2 = new GregorianCalendar();
                                    date2.setTime(t2.getEventDateTime());

                                    int yearDiff = Integer.compare(date1.get(Calendar.YEAR), date2.get(Calendar.YEAR));

                                    if (yearDiff != 0) return yearDiff;

                                    int monthDiff = Integer.compare(date1.get(Calendar.MONTH), date2.get(Calendar.MONTH));

                                    if (monthDiff != 0) return yearDiff;

                                    return Double.compare(score2, score1);
                                })
                                .collect(Collectors.toList());

                        ticketsList.postValue(tickets);
                        ticketListLoadingState.postValue(TicketListLoadingState.loaded);
                    }
                });

            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - refreshTicketsList - searchTickets - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_loading_tickets, Toast.LENGTH_LONG).show();
            }
        });
    }

    public Category getCategoryById(int categoryId) {
        Log.d("StuckWithTicketsLog", "Model - getCategoryById - " + categoryId);
        for (Category c : Objects.requireNonNull(categoriesList.getValue())
        ) {
            if (c.getId() == categoryId) {
                return c;
            }
        }
        return null;
    }

    public LiveData<List<Category>> getAllCategories() {
        Log.d("StuckWithTicketsLog", "Model - getAllCategories");
        if (categoriesList.getValue() == null) {
            refreshCategoriesList();
        }

        return categoriesList;
    }

    private void refreshCategoriesList() {
        Log.d("StuckWithTicketsLog", "Model - refreshCategoriesList");
        categoriesList.setValue(Collections.emptyList());
        modelRestApi.getAllCategories(new ModelRestApi.GetAllCategoriesListener() {
            @Override
            public void onComplete(List<Category> list) {
                Log.d("StuckWithTicketsLog", "Model - refreshCategoriesList - getAllCategories - onComplete");
                executor.execute(() -> {
                    if (list == null) {
                        Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.something_went_wrong_loading_categories, Toast.LENGTH_LONG).show();
                    } else {
                        categoriesList.postValue(list);
                    }
                });
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - refreshCategoriesList - getAllCategories - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_loading_categories, Toast.LENGTH_LONG).show();
            }
        });
    }

    public LiveData<Ticket> getTicketById() {
        return ticketById;
    }

    public LiveData<Ticket> refreshTicketById(int ticketId) {
        Log.d("StuckWithTicketsLog", "Model - getTicketById - " + ticketId);
        ticketById.setValue(null);
        modelRestApi.getTicketDetails(ticketId, new ModelRestApi.GetTicketDetailsListener() {
                    @Override
                    public void onComplete(Ticket ticket) {
                        Log.d("StuckWithTicketsLog", "Model - getTicketById - getTicketDetails - onComplete");
                        executor.execute(() -> {
                            if (ticket == null) {
                                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.something_went_wrong_loading_ticket, Toast.LENGTH_LONG).show();
                            } else {
                                ticketById.postValue(ticket);
                            }
                        });
                    }

                    @Override
                    public void onError(String message) {
                        Log.d("StuckWithTicketsLog", "Model - getTicketById - getTicketDetails - onError\n\t" + message);
                        Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_loading_ticket, Toast.LENGTH_LONG).show();
                    }
                }
        );
        return ticketById;
    }

    public LiveData<User> getContactInfoById(int userId) {
        Log.d("StuckWithTicketsLog", "Model - getContactInfoById - " + userId);
        contactInfoById.setValue(null);
        modelRestApi.getContactInfoById(userId, new ModelRestApi.GetContactInfoByIdListener() {
                    @Override
                    public void onComplete(User contactInfo) {
                        Log.d("StuckWithTicketsLog", "Model - getContactInfoById - getContactInfoById - onComplete");
                        executor.execute(() -> {
                            if (contactInfo == null) {
                                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.something_went_wrong_loading_contact_info, Toast.LENGTH_LONG).show();
                            } else {
                                contactInfoById.postValue(contactInfo);
                            }
                        });
                    }

                    @Override
                    public void onError(String message) {
                        Log.d("StuckWithTicketsLog", "Model - getContactInfoById - getContactInfoById - onError\n\t" + message);
                        Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_loading_contact_info, Toast.LENGTH_LONG).show();
                    }
                }
        );
        return contactInfoById;
    }

    public LiveData<User> getProfileInfo() {
        Log.d("StuckWithTicketsLog", "Model - getProfileInfo");
        profileInfo.setValue(null);
        modelRestApi.getProfileInfo(new ModelRestApi.GetProfileInfoListener() {
            @Override
            public void onComplete(User profileInfo) {
                Log.d("StuckWithTicketsLog", "Model - getProfileInfo - getProfileInfo - onComplete");
                executor.execute(() -> {
                    if (profileInfo == null) {
                        Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.something_went_wrong_loading_profile_info, Toast.LENGTH_LONG).show();
                    } else {
                        Model.this.profileInfo.postValue(profileInfo);
                    }
                });
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - getProfileInfo - getProfileInfo - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_loading_profile_info, Toast.LENGTH_LONG).show();
            }
        });
        return profileInfo;
    }

    public LiveData<Boolean> isLoggedIn() {
        return modelRestApi.isLoggedIn();
    }

    public LiveData<Integer> getCurrentUserId() {
        return modelRestApi.getCurrentUserId();
    }

    public LiveData<String> getCurrentUserEmail() {
        return modelRestApi.getCurrentUserEmail();
    }

    public interface RegisterListener {
        void onSuccess();

        void onFail(String message);
    }

    public void register(String firstName, String lastName, String email, String password, String phone, String imageId, String interests, Model.RegisterListener listener) {
        Log.d("StuckWithTicketsLog", "Model - register");
        modelRestApi.register(new RegisterRequestBody(firstName, lastName, email, password, phone, imageId, interests), new ModelRestApi.RegisterListener() {
            @Override
            public void onComplete() {
                Log.d("StuckWithTicketsLog", "Model - register - register - onComplete");
                listener.onSuccess();
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - register - register - onError\n\t" + message);
                listener.onFail(message);
            }
        });
    }

    public interface LoginListener {
        void onSuccess();

        void onFail(String message);
    }

    public void login(String email, String password, Model.LoginListener listener) {
        Log.d("StuckWithTicketsLog", "Model - login");
        modelRestApi.login(new LoginRequestBody(email, password), new ModelRestApi.LoginListener() {
            @Override
            public void onComplete() {
                Log.d("StuckWithTicketsLog", "Model - login - login - onComplete");
                listener.onSuccess();
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - login - login - onError\n\t" + message);
                listener.onFail(message);
            }
        });
    }

    public interface LogoutListener {
        void onAfter();
    }

    public void logout(LogoutListener listener) {
        Log.d("StuckWithTicketsLog", "Model - logout");
        modelRestApi.logout(new ModelRestApi.LogoutListener() {
            @Override
            public void onAfter() {
                listener.onAfter();;
            }
        });
    }

    public interface AddImageListener {
        void onComplete(String imageId);
    }

    public void addImage(Bitmap imageBitmap, AddImageListener listener) {
        Log.d("StuckWithTicketsLog", "Model - addImage");
        executor.execute(() -> {
            modelRestApi.addImage(imageBitmap, new ModelRestApi.AddImageListener() {
                @Override
                public void onComplete(String imageId) {
                    Log.d("StuckWithTicketsLog", "Model - addImage - addImage - onComplete");
                    if (imageId == null) {
                        Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.something_went_wrong_uploading_image, Toast.LENGTH_LONG).show();
                    } else {
                        listener.onComplete(imageId);
                    }
                }

                @Override
                public void onError(String message) {
                    Log.d("StuckWithTicketsLog", "Model - addImage - addImage - onError\n\t" + message);
                    Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_uploading_image, Toast.LENGTH_LONG).show();
                }
            });
        });
    }

    public interface SellTicketListener {
        void onComplete();
    }

    public void sellTicket(String title, int category, String description, Long eventTimestamp,
                           int amount, int price, String imageId, double x, double y, SellTicketListener listener) {
        Log.d("StuckWithTicketsLog", "Model - sellTicket");
        modelRestApi.sellTicket(title, category, description, eventTimestamp, amount, price, imageId, x, y, new ModelRestApi.SellTicketListener() {

            @Override
            public void onComplete() {
                Log.d("StuckWithTicketsLog", "Model - sellTicket - sellTicket - onComplete");
                refreshTicketsList();
                refreshMyTicketsList();
                listener.onComplete();
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - sellTicket - sellTicket - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_selling_ticket, Toast.LENGTH_LONG).show();
            }
        });
    }

    public LiveData<List<Ticket>> getMyTickets() {
        Log.d("StuckWithTicketsLog", "Model - getMyTickets");
        if (myTicketsList.getValue() == null) {
            refreshMyTicketsList();
        }

        return myTicketsList;
    }

    public void refreshMyTicketsList() {
        Log.d("StuckWithTicketsLog", "Model - refreshMyTicketsList");

        myTicketListLoadingState.setValue(TicketListLoadingState.loading);

        executor.execute(() -> {
            List<Ticket> myTickets = AppLocalDb.db.ticketDao().getAll()
                    .stream()
                    .filter(ticket -> getCurrentUserId().getValue() != null &&
                            ticket.getUserId() == getCurrentUserId().getValue())
                    .collect(Collectors.toList());
            Log.d("StuckWithTicketsLog", "Model - refreshMyTicketsList - Tickets from room: " + myTickets.size());
            myTicketsList.postValue(myTickets);
        });

        modelRestApi.getMyTickets(new ModelRestApi.GetMyTicketsListener() {
            @Override
            public void onComplete(List<Ticket> list) {
                Log.d("StuckWithTicketsLog", "Model - refreshMyTicketsList - getMyTickets - onComplete");
                executor.execute(() -> {
                    if (list == null) {
                        Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.something_went_wrong_loading_your_tickets, Toast.LENGTH_LONG).show();
                    } else {
                        List<Ticket> ticketsOrdered = list.stream().sorted((t1, t2) -> t1.getEventDateTime().compareTo(t2.getEventDateTime())).collect(Collectors.toList());
                        myTicketsList.postValue(ticketsOrdered);
                        myTicketListLoadingState.postValue(TicketListLoadingState.loaded);
                    }
                });
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - refreshMyTicketsList - getMyTickets - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_loading_your_tickets, Toast.LENGTH_LONG).show();
            }
        });
    }

    public interface EditTicketListener {
        void onComplete();
    }

    public void editTicket(int id, String title, int category, String description, Long eventTimestamp, int amount, int price, String imageId, double x, double y, EditTicketListener listener) {
        Log.d("StuckWithTicketsLog", "Model - editTicket");
        modelRestApi.editTicket(id, title, category, description, eventTimestamp, amount, price, imageId, x, y, new ModelRestApi.EditTicketListener() {
            @Override
            public void onComplete() {
                Log.d("StuckWithTicketsLog", "Model - editTicket - editTicket - onComplete");
                refreshTicketsList();
                refreshMyTicketsList();
                listener.onComplete();
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - editTicket - editTicket - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_updating_ticket, Toast.LENGTH_LONG).show();
            }
        });
    }

    public interface DeleteTicketListener {
        void onComplete();
    }

    public void deleteTicket(int id, DeleteTicketListener listener) {
        Log.d("StuckWithTicketsLog", "Model - deleteTicket");
        modelRestApi.deleteTicket(id, new ModelRestApi.DeleteTicketListener() {
            @Override
            public void onComplete() {
                Log.d("StuckWithTicketsLog", "Model - deleteTicket - deleteTicket - onComplete");
                refreshTicketsList();
                refreshMyTicketsList();
                listener.onComplete();
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - deleteTicket - deleteTicket - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_deleting_ticket, Toast.LENGTH_LONG).show();
            }
        });
    }

    public interface EditProfileListener {
        void onComplete();
    }

    public void editProfile(int id, String firstName, String lastName, String phoneNumber, String imageId, String interests, EditProfileListener listener) {
        Log.d("StuckWithTicketsLog", "Model - editProfile");
        modelRestApi.editProfile(id, firstName, lastName, phoneNumber, imageId, interests, new ModelRestApi.EditProfileListener() {
            @Override
            public void onComplete() {
                Log.d("StuckWithTicketsLog", "Model - editProfile - editProfile - onComplete");
                listener.onComplete();
            }

            @Override
            public void onError(String message) {
                Log.d("StuckWithTicketsLog", "Model - editProfile - editProfile - onError\n\t" + message);
                Toast.makeText(StuckWithTicketsApplication.getContext(), R.string.error_occurred_updating_profile, Toast.LENGTH_LONG).show();
            }
        });
    }


}
