package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

public class EditProfileRequestBody {
    final String firstName;
    final String lastName;
    final String phoneNumber;
    final String imageId;
    final String interests;

    public EditProfileRequestBody(String firstName, String lastName, String phoneNumber, String imageId, String interests) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.imageId = imageId;
        this.interests = interests;

    }
}
