package com.dreamteam109.stuckwithtickets.ui.filter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.dreamteam109.stuckwithtickets.model.Model;

import java.util.List;

public class FilterViewModel extends ViewModel {

    private MutableLiveData<Integer> priceLimit = new MutableLiveData<Integer>();
    private MutableLiveData<Integer> amount = new MutableLiveData<Integer>();
    private MutableLiveData<Long> fromDate = new MutableLiveData<Long>();
    private MutableLiveData<Long> toDate = new MutableLiveData<Long>();
    private MutableLiveData<List<Integer>> categories = new MutableLiveData<List<Integer>>();

    public LiveData<List<Category>> getCategoriesList() {
        return Model.instance.getAllCategories();
    }

    public List<String> getSelectedCategories() {
        return Model.instance.getSelectedCategories();
    }

    public void setSelectedCategories(List<Integer> categories) {
        this.categories.setValue(categories);
    }

    public void setFromDate(Long fromDate) {
        this.fromDate.setValue(fromDate);
    }

    public void setToDate(Long toDate) {
        this.toDate.setValue(toDate);
    }

    public Integer getPriceLimit() {
        return Model.instance.getPriceLimit();
    }

    public void setPriceLimit(Integer priceLimit) {
        this.priceLimit.setValue(priceLimit);
    }

    public Integer getAmount() {
        return Model.instance.getAmount();
    }

    public void setAmount(Integer amount) {
        this.amount.setValue(amount);
    }

    public Long getStartTimestamp() {
        return Model.instance.getStartTimestamp();
    }

    public Long getEndTimestamp() {
        return Model.instance.getEndTimeStamp();
    }

    public void applyChanges() {
        Model.instance.setStartTimestamp(fromDate.getValue());
        Model.instance.setEndTimestamp(toDate.getValue());
        Model.instance.setPriceLimit(priceLimit.getValue());
        Model.instance.setAmount(amount.getValue());
        Model.instance.setSelectedCategories(categories.getValue());
    }

    public void resetChanges() {
        setPriceLimit(null);
        setSelectedCategories(null);
        setAmount(null);
        setToDate(null);
        setFromDate(null);
        applyChanges();
    }
}