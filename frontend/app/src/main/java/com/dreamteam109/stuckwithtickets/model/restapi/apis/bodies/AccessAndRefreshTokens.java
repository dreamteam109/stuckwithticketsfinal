package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

public class AccessAndRefreshTokens {
    String accessToken;
    String refreshToken;

    public AccessAndRefreshTokens(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
