package com.dreamteam109.stuckwithtickets.model;

import android.util.Patterns;

public class Validators {
    public Validators() {

    }

    public static boolean isEmailValid(String email) {
        return email != null && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }

    public static boolean isPhoneValid(String phone) {
        return phone != null && Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isNotEmptyString(String str) {
        return str != null && str.length() > 0;
    }

    public static boolean isNotNull(Object obj) {
        return obj != null;
    }
}