package com.dreamteam109.stuckwithtickets.model.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

@Entity
public class Ticket {
    @SerializedName("id")
    @PrimaryKey
    @NonNull
    public int id;

    @SerializedName("userId")
    public int userId;

    @SerializedName("eventName")
    public String eventName;

    @SerializedName("categoryId")
    public int categoryId;

    @SerializedName("categoryName")
    public String categoryName;

    @SerializedName("description")
    public String description;

    @SerializedName("eventDatetime")
    public Date eventDateTime;

    @SerializedName("price")
    public int price;

    @SerializedName("amount")
    public int amount;

    @SerializedName("imageId")
    public String imageId;

    @SerializedName("x")
    public double x;

    @SerializedName("y")
    public double y;

    @SerializedName("isDeleted")
    public boolean isDeleted;

    @SerializedName("lastUpdateDate")
    public Date lastUpdateDate;

    @SerializedName("score")
    public double score;


    public Ticket() {

    }

    public Ticket(int id, int userId, String eventName, int categoryId, String categoryName,
                  String description, Date eventDateTime, int price, int amount, String imageId, double x, double y, boolean isDeleted, Date lastUpdateDate, double score) {
        this.id = id;
        this.userId = userId;
        this.eventName = eventName;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.description = description;
        this.eventDateTime = eventDateTime;
        this.price = price;
        this.amount = amount;
        this.imageId = imageId;
        this.x = x;
        this.y = y;
        this.isDeleted = isDeleted;
        this.lastUpdateDate = lastUpdateDate;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEventDateTime() {
        return eventDateTime;
    }

    public void setEventDateTime(Date eventDateTime) {
        this.eventDateTime = eventDateTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public LatLng getLocationPnt() {
        return new LatLng(this.x, this.y);
    }

    public double getScore() {
        return score;
    }
}
