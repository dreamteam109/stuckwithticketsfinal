package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

public class AddImageResponseBody {
    String imageId;

    public AddImageResponseBody(String imageId) {
        this.imageId = imageId;
    }

    public String getImageId() {
        return imageId;
    }
}
