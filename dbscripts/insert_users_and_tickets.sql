-- NOTE: All the following encrypted passwords are '123456'
insert into stuckwithtickets.users (first_name, last_name, email, password, phone_number, image_id, interests)
values ('Itay', 'Mastor', 'itay647@gmail.com', '$2a$10$vqwphg/jKwvSNPnl8yGUGe08K8vIhH.zQAP1bNO9UtvFBZvifzaC6', '0501234567', '6b0383e6b86b6c9577972851f9c453cb', 'I like rock music'), -- id: 1
       ('Raz', 'Vikelman', 'razvikel@gmail.com', '$2a$10$D9mIey9D3XxIHKPXu82seeNWXtEyblITTmLrShy8LAJxQg1Oe5B5m', '0511234567', '4fddab496efad0a5cbc99251ab5b75e8', 'I love Aviv Gefen'), -- id: 2
       ('Linoy', 'Yosef', 'yosef.linoy@gmail.com', '$2a$10$Ne2ukeobEi0dKQYXY4oh2eH6zrmIBPs9/pvSPojyQXNu3jSLmJC0K', '0521234567', 'e49bb982bd2e419682565e285a899a2f', 'I like comedy'), -- id: 3
       ('Inbal', 'Yosef', 'yosef.inbal@gmail.com', '$2a$10$4y02mGU2l64BXEOHO/ryi.Q3embHgyA4Q6xkxLeuC/3a0sV5KcxE6', '0531234567', '17a484372be38a9f88b65b0b6ae70357', 'I live in Israel and like music'); -- id: 4

insert into stuckwithtickets.tickets (user_id, event_name, category_id, description, event_datetime, price, amount, image_id, x, y)
values
       (1, 'Idan Raichel', 1, 'description ... IDK ...', now() + '24 days'::interval, 100, 2, '5247f821ead38c5f3563a3494ea370de', 32.1775173825639, 34.903999304598024),
       (2, 'Idan Amedi', 1, 'description ... IDK ...', now() + '41 days'::interval, 100, 2, 'c2bf93591210195af2de1081fcc2fa5d', 32.326862330202346, 34.86189617285916),
       (3, 'Aviv Geffen', 1, 'description ... IDK ...', now() + '30 days'::interval, 100, 2, 'be1daf1cf1a258b5180a8661f0fd2158', 32.8116632255483, 34.98525490279606),
       (4, 'Idan Rafael Haviv', 1, 'description ... IDK ...', now() + '85 days'::interval, 100, 2, 'a193dbcc9bcf6f1c9f78a9b201a9b3e7', 32.79258062475068, 35.54131432815427),
       (1, 'Ivri Lider', 1, 'description ... IDK ...', now() + '100 days'::interval, 100, 2, '1414e7107aed1cdcd476215f26b72e3e', 33.19896894866688, 35.5772101004657),
       (2, 'Miri Mesika', 1, 'description ... IDK ...', now() + '16 days'::interval, 100, 2, '8a9a1131bc21615a2ad067a616ab76a1', 29.554307693522496, 34.96545105569777),
       (3, 'Yoni Bloch', 1, 'description ... IDK ...', now() + '27 days'::interval, 150, 2, '8f55bf8d47c25678c62243c2c5a5dc8a', 31.24304108098204, 34.810882303509324),
       (3, 'Some event 2', 2, 'description ... bla ...', now() + '200 days'::interval, 200, 3, null, 31.785438607139678, 35.21261168655508),
       (4, 'Some event 3', 3, 'description ... blabla11 ...', now() + '150 days'::interval, 300, 4, null, 32.18087178235566, 34.87106917685389),
       (1, 'Some event 4', 4, 'description ... test123 ...', now() + '120 days'::interval, 400, 2, null, 32.077370437612444, 34.78600802929589),
       (2, 'Some event 5', 5, 'description ... things ...', now() + '180 days'::interval, 500, 3, null, 31.751207705772206, 35.1906304021422),
       (3, 'Some event 6', 2, 'description ... hello world ...', now() + '210 days'::interval, 600, 4, null, 30.608384369856832, 34.80650040406175),
       (4, 'Some event 7', 3, 'description ... testing ...' ||E'\n'|| 'containing new lines' ||E'\n'|| 'another line',
            now() + '160 days'::interval, 700, 2, null, 31.792794043287003, 34.636517039290325),
       (1, 'Some event 8', 4, 'description ... yea I''m not very creative ... this is a very long description. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam euismod sed velit quis sodales. Cras eget velit ornare, ultricies ligula id, maximus justo.',
            now() + '130 days'::interval, 800, 3, null, 32.514691767237714, 34.906925653687196);


insert into stuckwithtickets.users_and_tickets_scores (user_id, ticket_id, score)
values (1, 2, 0.5),
       (2,4, 0.1),
       (3,10, 0.3),
       (4,8, 0.7),
       (1,6, 0.8)
       
