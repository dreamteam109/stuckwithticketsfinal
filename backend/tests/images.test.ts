import { getImageHandler } from "../src/Images/Images.handlers";
import { getMockReq, getMockRes } from "@jest-mock/express";
import { ImageRecord } from "../src/db/types";

describe("Images handlers tests", () => {
  describe("getImageHandler", () => {
    test("return image if exist", async () => {
      const req = getMockReq({ params: { id: "2" } });
      const res = getMockRes().res;
      const imageRecord: ImageRecord = {
        id: "1",
        mimeType: "mime Type",
        imageData: "Image data",
      };
      const getImageByIdMock = jest.fn(async (id) => ({ ...imageRecord, id }));
      await getImageHandler(getImageByIdMock)(req, res);
      expect(res.type).toHaveBeenCalledWith(imageRecord.mimeType);
      expect(res.end).toHaveBeenCalledWith(imageRecord.imageData);
      expect(getImageByIdMock).toBeCalledWith("2");
    });

    test("return 404 if not exist", async () => {
        const req = getMockReq({ params: { id: "1" } });
        const res = getMockRes().res;
       
        const getImageByIdMock = jest.fn(async (id) => {});
        await getImageHandler(getImageByIdMock)(req, res);
        expect(res.sendStatus).toHaveBeenCalledWith(404);
        expect(getImageByIdMock).toBeCalledWith("1");
      });
  });
});
