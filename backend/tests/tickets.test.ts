import { getMockReq, getMockRes } from "@jest-mock/express";
import {
  deleteTicketHandler,
  editTicketHandler,
  getMyTicketsHandler,
  getTicketDetails,
  searchTicketsHandler,
  sellTicketHandler,
} from "../src/Tickets/Tickets.handlers";
import { User } from "../src/Users/Users.types";

describe("Tickets handlers tests", () => {
  describe("editTicketHandler", () => {
    test("valid ticket and exist - edit ticket", async () => {
      const body = {
        title: "title ticket",
        category: 2,
        description: "description",
        eventTimestamp: 3000,
        price: 30,
        amount: 2,
        imageId: "3",
        x: 10,
        y: 20,
      };
      const req = getMockReq({ body, params: { id: 2 } });
      const res = getMockRes().res;

      const editTicketMock = jest.fn(
        async (
          id,
          title,
          category,
          description,
          eventTimestamp,
          price,
          amount,
          imageId,
          x,
          y
        ) => {}
      );

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await editTicketHandler(editTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(editTicketMock).toHaveBeenCalledWith(
        req.params.id,
        body.title,
        body.category,
        body.description,
        body.eventTimestamp,
        body.price,
        body.amount,
        body.imageId,
        body.x,
        body.y
      );
      expect(res.send).toHaveBeenCalled();
    });

    test("invalid ticket - returns 400", async () => {
      const body = {
        category: 2,
        description: "description",
        eventTimestamp: 3000,
        price: 30,
        amount: 2,
        imageId: "3",
        x: 10,
        y: 20,
      };
      const req = getMockReq({ body });
      const res = getMockRes().res;

      const editTicketMock = jest.fn(
        async (
          id,
          title,
          category,
          description,
          eventTimestamp,
          price,
          amount,
          imageId,
          x,
          y
        ) => {}
      );

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await editTicketHandler(editTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
    test("undefined ticket - returns 400", async () => {
      const req = getMockReq();
      const res = getMockRes().res;
      const editTicketMock = jest.fn(async () => {});

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await editTicketHandler(editTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
  });

  describe("getMyTicketsHandler", () => {
    test("valid user - returns it's tickets", async () => {
      const user: User = {
        id: 1,
        firstName: "ploni",
        lastName: "almoni",
        email: "whatever@gmail.com",
        phoneNumber: "0501234567",
        password: "pass",
        isDeleted: false,
        lastUpdateDate: new Date(),
        creationDate: new Date(),
        interests: "I like rock music",
      };
      const req = getMockReq({ user });
      const res = getMockRes().res;

      const tickets = [
        {
          id: 2,
          userId: 1,
          eventName: "eventName",
          categoryId: "3",
          name: "categoryName",
          description: "description",
          event_datetime: 1000,
          price: 100,
          amount: 2,
          imageId: 4,
          x: 20,
          y: 10,
          isDeleted: false,
          lastUpdateDate: 2000,
        },
        {
          id: 4,
          userId: 1,
          eventName: "eventName2",
          categoryId: "1",
          name: "categoryName",
          description: "description",
          event_datetime: 2000,
          price: 50,
          amount: 1,
          imageId: 1,
          x: 10,
          y: 10,
          isDeleted: true,
          lastUpdateDate: 1000,
        },
      ];
      const getMyTicketsMock = jest.fn(async (user) => tickets);
      await getMyTicketsHandler(getMyTicketsMock)(req, res);
      expect(res.json).toHaveBeenCalledWith(tickets);
    });

    test("invalid user - returns 400", async () => {
      const user: User = {
        id: 1,
        firstName: "",
        lastName: "",
        email: "whatever@gmail.com",
        phoneNumber: "0501234567",
        password: "pass",
        isDeleted: false,
        lastUpdateDate: new Date(),
        creationDate: new Date(),
        interests: "I like rock music",
      };
      const req = getMockReq({ user });
      const res = getMockRes().res;
      const getMyTicketsMock = jest.fn(async (user) => ({}));
      await getMyTicketsHandler(getMyTicketsMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
    test("undefined user - returns 400", async () => {
      const req = getMockReq({});
      const res = getMockRes().res;
      const getMyTicketsMock = jest.fn(async (user) => ({}));
      await getMyTicketsHandler(getMyTicketsMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
  });

  test("getTicketDetails", async () => {
    const req = getMockReq({ params: { id: 2 } });
    const res = getMockRes().res;
    const ticket = {
      id: 2,
      userId: 1,
      eventName: "eventName",
      categoryId: "3",
      name: "categoryName",
      description: "description",
      event_datetime: 1000,
      price: 50,
      amount: 2,
      imageId: 4,
      x: 10,
      y: 10,
      lastUpdateDate: 2000,
    };
    const getTicketDetailsMock = jest.fn(async (id) => ({ ...ticket, id }));
    await getTicketDetails(getTicketDetailsMock)(req, res);
    expect(res.json).toHaveBeenCalledWith(ticket);
  });
  describe("sellTicketHandler", () => {
    test("valid ticket and user - sell ticket", async () => {
      const body = {
        title: "title ticket",
        category: 2,
        description: "description",
        eventTimestamp: 3000,
        price: 30,
        amount: 2,
        imageId: "3",
        x: 10,
        y: 20,
      };
      const user: User = {
        id: 1,
        firstName: "ploni",
        lastName: "almoni",
        email: "whatever@gmail.com",
        phoneNumber: "0501234567",
        password: "pass",
        isDeleted: false,
        lastUpdateDate: new Date(),
        creationDate: new Date(),
        interests: "I like rock music",
      };
      const req = getMockReq({ body, user });
      const res = getMockRes().res;

      const sellTicketMock = jest.fn(
        async (
          id,
          amount,
          category,
          description,
          eventTimestamp,
          price,
          title,
          imageId,
          x,
          y
        ) => 1
      );

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await sellTicketHandler(sellTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(sellTicketMock).toHaveBeenCalledWith(
        user.id,
        body.title,
        body.category,
        body.description,
        body.eventTimestamp,
        body.price,
        body.amount,
        body.imageId,
        body.x,
        body.y
      );
      expect(res.send).toHaveBeenCalled();
    });
    test("invalid ticket - return 400", async () => {
      const body = {
        title: "",
        category: 2,
        description: "description",
        eventTimestamp: 3000,
        price: 30,
        amount: 2,
        imageId: "3",
        x: 10,
        y: 20,
      };
      const user: User = {
        id: 1,
        firstName: "ploni",
        lastName: "almoni",
        email: "whatever@gmail.com",
        phoneNumber: "0501234567",
        password: "pass",
        isDeleted: false,
        lastUpdateDate: new Date(),
        creationDate: new Date(),
        interests: "I like rock music",
      };
      const req = getMockReq({ body, user });
      const res = getMockRes().res;

      const sellTicketMock = jest.fn(
        async (
          id,
          amount,
          category,
          description,
          eventTimestamp,
          price,
          title,
          imageId,
          x,
          y
        ) => 1
      );

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await sellTicketHandler(sellTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
    test("invalid user - return 400", async () => {
      const body = {
        title: "title",
        category: 2,
        description: "description",
        eventTimestamp: 3000,
        price: 30,
        amount: 2,
        imageId: "3",
        x: 10,
        y: 20,
      };
      const user: User = {
        id: 1,
        firstName: "",
        lastName: "almoni",
        email: "whatever@gmail.com",
        phoneNumber: "0501234567",
        password: "pass",
        isDeleted: false,
        lastUpdateDate: new Date(),
        creationDate: new Date(),
        interests: "I like rock music",
      };
      const req = getMockReq({ body, user });
      const res = getMockRes().res;

      const sellTicketMock = jest.fn(
        async (
          id,
          amount,
          category,
          description,
          eventTimestamp,
          price,
          title,
          imageId,
          x,
          y
        ) => 1
      );

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await sellTicketHandler(sellTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
    test("missing ticket - return 400", async () => {
      const user: User = {
        id: 1,
        firstName: "ploni",
        lastName: "almoni",
        email: "whatever@gmail.com",
        phoneNumber: "0501234567",
        password: "pass",
        isDeleted: false,
        lastUpdateDate: new Date(),
        creationDate: new Date(),
        interests: "I like rock music",
      };
      const req = getMockReq({ user });
      const res = getMockRes().res;

      const sellTicketMock = jest.fn(
        async (
          id,
          amount,
          category,
          description,
          eventTimestamp,
          price,
          title,
          imageId,
          x,
          y
        ) => 1
      );

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await sellTicketHandler(sellTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
    test("missing user - return 400", async () => {
      const body = {
        title: "title",
        category: 2,
        description: "description",
        eventTimestamp: 3000,
        price: 30,
        amount: 2,
        imageId: "3",
        x: 10,
        y: 20,
      };
      const req = getMockReq({ body });
      const res = getMockRes().res;

      const sellTicketMock = jest.fn(
        async (
          id,
          amount,
          category,
          description,
          eventTimestamp,
          price,
          title,
          imageId,
          x,
          y
        ) => 1
      );

      const updateScoresForTicketsAndUsersMock = jest.fn(
        async (
          ticketIds,
          userIds
        ) => {}
      );

      await sellTicketHandler(sellTicketMock, updateScoresForTicketsAndUsersMock)(req, res);
      expect(res.status).toHaveBeenCalledWith(400);
    });
  });
  test("getTicketDetails", async () => {
    const req = getMockReq({ params: { id: 2 } });
    const res = getMockRes().res;
    const ticket = {
      id: 2,
      userId: 1,
      eventName: "eventName",
      categoryId: "3",
      name: "categoryName",
      description: "description",
      event_datetime: 1000,
      price: 50,
      amount: 2,
      imageId: 4,
      x: 10,
      y: 10,
      lastUpdateDate: 2000,
    };
    const getTicketDetailsMock = jest.fn(async (id) => ({ ...ticket, id }));
    await getTicketDetails(getTicketDetailsMock)(req, res);
    expect(res.json).toHaveBeenCalledWith(ticket);
  });
  describe("searchTicketsHandler", () => {
    test("valid ticket request - return ticket if exist", async () => {
      const body = {
        lastUpdateTimestamp: 3000,
        categories: [1, 2, 3],
        endTimestamp: 4000,
        priceLimit: 100,
        startTimestamp: 1000,
        text: "text",
        amount: 2,
      };

      const req = getMockReq({ body });
      const res = getMockRes().res;

      const searchTicketMock = jest.fn(
        async (
          lastUpdateTimestamp,
          text,
          categories,
          priceLimit,
          amount,
          startTimestamp,
          endTimestamp
        ) => {}
      );
      await searchTicketsHandler(searchTicketMock)(req, res);
      expect(searchTicketMock).toHaveBeenCalledWith(
        body.lastUpdateTimestamp,
        body.text,
        body.categories,
        body.priceLimit,
        body.amount,
        body.startTimestamp,
        body.endTimestamp
      );
      expect(res.json).toHaveBeenCalled();
    });
    test("invalid ticket request - return 400", async () => {
      const body = {
        lastUpdateTimestamp: 3000,
        categories: "poerty",
        endTimestamp: 4000,
        priceLimit: 100,
        startTimestamp: 1000,
        text: "text",
        amount: 2,
      };

      const req = getMockReq({ body });
      const res = getMockRes().res;

      const searchTicketMock = jest.fn(
        async (
          lastUpdateTimestamp,
          text,
          categories,
          priceLimit,
          amount,
          startTimestamp,
          endTimestamp
        ) => {}
      );
      await searchTicketsHandler(searchTicketMock)(req, res);
      expect(searchTicketMock).toHaveBeenCalledWith(
        body.lastUpdateTimestamp,
        body.text,
        body.categories,
        body.priceLimit,
        body.amount,
        body.startTimestamp,
        body.endTimestamp
      );
      expect(res.json).toHaveBeenCalled();
    });
  });
  describe("deleteTicketHandler", () => {
    test("valid user and tickets exist and belongs to user - delete ticket", async () => {
      const user: User = {
        id: 2,
        firstName: "ploni",
        lastName: "almoni",
        email: "whatever@gmail.com",
        phoneNumber: "0501234567",
        password: "pass",
        isDeleted: false,
        lastUpdateDate: new Date(),
        creationDate: new Date(),
        interests: "I like rock music",
      };
      const ticket = {
        amount: 3,
        categoryId: 3,
        description: "desc",
        eventDatetime: 200,
        eventName: "name",
        id: 2,
        imageId: 1,
        lastUpdateDate: 300,
        nameCategory: "poetry",
        price: 20,
        userId: 2,
        x: 10,
        y: 20,
      };
      const req = getMockReq({ user, params: { id: 2 } });
      const res = getMockRes().res;

      const ticketByIdMock = jest.fn(async (  id ) => { return ticket});
      const deleteTicketMock = jest.fn(async ( id ) => {});
      const removeTicketFromScores = jest.fn(async ( id ) => {});

      await deleteTicketHandler(ticketByIdMock, deleteTicketMock, removeTicketFromScores)(req, res);
      expect(ticketByIdMock).toHaveBeenCalledWith(ticket.id);
      expect(deleteTicketMock).toHaveBeenCalledWith(ticket.id);
      expect(removeTicketFromScores).toHaveBeenCalledWith(ticket.id);

      expect(res.send).toHaveBeenCalled();
    });
  });
});
