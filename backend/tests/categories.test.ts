import { getCategoriesHandler } from "../src/Categories/Categories.handlers";
import { getMockReq, getMockRes } from '@jest-mock/express'

describe("Categories handlers tests", () => {
    test("getCategoriesHandler", async () => {
        const req = getMockReq();
        const res = getMockRes().res;
        const categories = [
            {id: 1, name: "sport"},
            {id: 2, name: "culture"}
        ]
        const getAllCategoriesMock = jest.fn(async () => categories);
        await getCategoriesHandler(getAllCategoriesMock)(req, res);
        expect(res.json).toHaveBeenCalledWith(categories);
    })
})
