import { getMockReq, getMockRes } from '@jest-mock/express'
import { ContactInfo } from '../src/db/users';
import { contactInfoHandler, editUserDetailsHandler, getMyUserDetailsHandler } from "../src/Users/Users.handlers";
import { EditUserDetailsRequestBody, User, UserForEdit } from '../src/Users/Users.types';

describe("Users handlers tests", () => {
    describe("contactInfoHandler", () => {
        test("contactInfoHandler", async () => {
            const req = getMockReq({ params: { id: "2" } });
            const res = getMockRes().res;
            const contactInfo: ContactInfo = {
                id: 1,
                firstName: "ploni",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567"
            };
            const getContactInfoByIdMock = jest.fn(async id => ({ ...contactInfo, id }));
            await contactInfoHandler(getContactInfoByIdMock)(req, res);
            expect(res.json).toHaveBeenCalledWith({...contactInfo, id: 2});
        })
    })

    describe("getMyUserDetailsHandler", () => {
        test("valid user - returns the user", async () => {
            const user: User = {
                id: 1,
                firstName: "ploni",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567",
                password: 'pass',
                isDeleted: false,
                lastUpdateDate: new Date(),
                creationDate: new Date(),
                interests: "I like rock music"
            }
            const req = getMockReq({ user });
            const res = getMockRes().res;
            await getMyUserDetailsHandler(req, res);
            expect(res.send).toHaveBeenCalledWith(user);
        })

        test("invalid user - returns 400", async () => {
            const user: User = {
                id: 1,
                firstName: "",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567",
                password: 'pass',
                isDeleted: false,
                lastUpdateDate: new Date(),
                creationDate: new Date(),
                interests: "I like rock music"
            }
            const req = getMockReq({ user });
            const res = getMockRes().res;
            await getMyUserDetailsHandler(req, res);
            expect(res.status).toHaveBeenCalledWith(400);
        })

        test("undefined user - returns 400", async () => {
            const req = getMockReq();
            const res = getMockRes().res;
            await getMyUserDetailsHandler(req, res);
            expect(res.status).toHaveBeenCalledWith(400);
        })
    })

    describe("editUserDetailsHandler", () => {
        test("valid details - return success", async () => {
            const body: EditUserDetailsRequestBody = {
                firstName: "ploni",
                lastName: "almoni",
                phoneNumber: "0551234567",
                imageId: "123",
                interests: "I like rock music"
            }
            const user: User = {
                id: 1,
                firstName: "ploni",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567",
                password: 'pass',
                isDeleted: false,
                lastUpdateDate: new Date(),
                creationDate: new Date(),
                interests: "I like rock music"
            }
            const req = getMockReq({ body, user });
            const res = getMockRes().res;
            const userForEdit: UserForEdit = {
                id: 1,
                ...body
            };
            const editUserDetailsMock = jest.fn(async user => {});

            const updateScoresForTicketsAndUsersMock = jest.fn(
                async (
                  ticketIds,
                  userIds
                ) => {}
              );

            await editUserDetailsHandler(editUserDetailsMock, updateScoresForTicketsAndUsersMock)(req, res);
            expect(editUserDetailsMock).toHaveBeenCalledWith(userForEdit);
            expect(res.send).toHaveBeenCalled();
        })

        test("valid details without image - return success", async () => {
            const body: EditUserDetailsRequestBody = {
                firstName: "ploni",
                lastName: "almoni",
                phoneNumber: "0551234567",
                interests: "I like rock music"
            }
            const user: User = {
                id: 1,
                firstName: "ploni",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567",
                password: 'pass',
                isDeleted: false,
                lastUpdateDate: new Date(),
                creationDate: new Date(),
                imageId: "123",
                interests: "I like rock music"
            }
            const req = getMockReq({ body, user });
            const res = getMockRes().res;
            const userForEdit: UserForEdit = {
                id: 1,
                ...body
            };
            const editUserDetailsMock = jest.fn(async user => {});
            const updateScoresForTicketsAndUsersMock = jest.fn(
                async (
                  ticketIds,
                  userIds
                ) => {}
              );
            await editUserDetailsHandler(editUserDetailsMock, updateScoresForTicketsAndUsersMock)(req, res);
            expect(editUserDetailsMock).toHaveBeenCalledWith(userForEdit);
            expect(res.send).toHaveBeenCalled();
        })

        test("invalid details - return 400", async () => {
            const body: EditUserDetailsRequestBody = {
                firstName: "",
                lastName: "almoni",
                phoneNumber: "0551234567",
                interests: "I like rock music"
            }
            const user: User = {
                id: 1,
                firstName: "ploni",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567",
                password: 'pass',
                isDeleted: false,
                lastUpdateDate: new Date(),
                creationDate: new Date(),
                imageId: "123",
                interests: "I like rock music"
            }
            const req = getMockReq({ body, user });
            const res = getMockRes().res;
            const editUserDetailsMock = jest.fn(async user => {});
            const updateScoresForTicketsAndUsersMock = jest.fn(
                async (
                  ticketIds,
                  userIds
                ) => {}
              );
            await editUserDetailsHandler(editUserDetailsMock, updateScoresForTicketsAndUsersMock)(req, res);
            expect(res.status).toHaveBeenCalledWith(400);
        })

        test("invalid user - return 400", async () => {
            const body: EditUserDetailsRequestBody = {
                firstName: "ploni",
                lastName: "almoni",
                phoneNumber: "0551234567",
                interests: "I like rock music"
            }
            const user: User = {
                id: 1,
                firstName: "",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567",
                password: 'pass',
                isDeleted: false,
                lastUpdateDate: new Date(),
                creationDate: new Date(),
                imageId: "123",
                interests: "I like rock music"
            }
            const req = getMockReq({ body, user });
            const res = getMockRes().res;
            const editUserDetailsMock = jest.fn(async user => {});
            const updateScoresForTicketsAndUsersMock = jest.fn(
                async (
                  ticketIds,
                  userIds
                ) => {}
              );
            await editUserDetailsHandler(editUserDetailsMock, updateScoresForTicketsAndUsersMock)(req, res);
            expect(res.status).toHaveBeenCalledWith(400);
        })

        test("missing user - return 400", async () => {
            const body: EditUserDetailsRequestBody = {
                firstName: "ploni",
                lastName: "almoni",
                phoneNumber: "0551234567",
                interests: "I like rock music"
            }
            const req = getMockReq({ body });
            const res = getMockRes().res;
            const editUserDetailsMock = jest.fn(async user => {});
            const updateScoresForTicketsAndUsersMock = jest.fn(
                async (
                  ticketIds,
                  userIds
                ) => {}
              );
            await editUserDetailsHandler(editUserDetailsMock, updateScoresForTicketsAndUsersMock)(req, res);
            expect(res.status).toHaveBeenCalledWith(400);
        })

        test("missing details - return 400", async () => {
            const user: User = {
                id: 1,
                firstName: "",
                lastName: "almoni",
                email: "whatever@gmail.com",
                phoneNumber: "0501234567",
                password: 'pass',
                isDeleted: false,
                lastUpdateDate: new Date(),
                creationDate: new Date(),
                imageId: "123",
                interests: "I like rock music"
            }
            const req = getMockReq({ user });
            const res = getMockRes().res;
            const editUserDetailsMock = jest.fn(async user => {});
            const updateScoresForTicketsAndUsersMock = jest.fn(
                async (
                  ticketIds,
                  userIds
                ) => {}
              );
            await editUserDetailsHandler(editUserDetailsMock, updateScoresForTicketsAndUsersMock)(req, res);
            expect(res.status).toHaveBeenCalledWith(400);
        })
    })
})
