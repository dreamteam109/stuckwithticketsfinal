import { Request, Response } from "express";
import { promises as fs } from "fs";
import path from "path";
import { getImageById, uploadImage } from "../db/images";
import { ImageRecord } from "../db/types";
import multer, { FileFilterCallback } from "multer";

const TEMP_IMAGES_FOLDER_PATH = path.join(__dirname, "../../_temp_images_for_upload_/");

export const createMulterMiddleware = () => {
    const upload = multer({
        dest: TEMP_IMAGES_FOLDER_PATH,
        fileFilter: (req: Request, file: Express.Multer.File, cb: FileFilterCallback) => {
            // TODO: consider removing png (only allowing jpeg images), because png files are too big.
            if (!file.mimetype.match(/^image\/(png|jpeg)$/)) {
                // upload only png and jpg format
                return cb(new Error('Please upload an image (png or jpeg)'));
            }
            cb(null, true);
        }
    });

    return upload.single('image');
}

export const addImageHandler = async (req: Request, res: Response) => {
    // HOW TO USE IN POSTMAN FOR EXMAPLE:
    // set the body to form-data, with key `image` and value - choose a file of type jpg

    // req.file is the `image` file
    console.log(req.file);
    /*
        This will log for example:
        {
            fieldname: 'image',
            originalname: '2.jpeg',
            encoding: '7bit',
            mimetype: 'image/jpeg',
            destination: '_temp_images_for_upload_',
            filename: '69d9339dacdca6aca72cd15d99c1c543',
            path: '_temp_images_for_upload_\\69d9339dacdca6aca72cd15d99c1c543',
            size: 18009
        }
    */
    
    const tempImageFilePath = path.join(TEMP_IMAGES_FOLDER_PATH, req.file.filename);
    const imageData = await fs.readFile(
        tempImageFilePath,
        {encoding: "hex"}
    );
    const imageRecord: ImageRecord = {
        id: req.file.filename,
        mimeType: req.file.mimetype,
        imageData: "\\x" + imageData
    };
    await uploadImage(imageRecord);
    await fs.rm(tempImageFilePath);

    res.send({ imageId: imageRecord.id });
}

export const getImageHandler = (getImageById: any) => async (req: Request, res: Response) => {
    const { id } = req.params;
    const imageRecord = await getImageById(id);
    if (imageRecord) {
        res.type(imageRecord.mimeType).end(imageRecord.imageData);
    } else {
        res.sendStatus(404);
    }
}
