import { Router, Request } from "express"
import { authMiddleware } from "../auth/auth";
import { getImageById } from "../db/images";
import { addImageHandler, createMulterMiddleware, getImageHandler } from "./Images.handlers";

/**
* @swagger
* tags:
*   name: Images
*   description: The Images API
*/

export const createImagesRouter = () => {
    const router = Router();

    /**
    * @swagger
    * /images/add:
    *   post:
    *     summary: Upload a new image
    *     tags: [Images]
    *     requestBody:
    *       required: true
    *       content:
    *         multipart/form-data:
    *           schema:
    *             type: object
    *             properties:
    *               image:
    *                 type: string
    *                 format: binary
    *             required:
    *               - image
    *     responses:
    *       200:
    *         description: Image added successfully
    *         content:
    *           application/json:
    *             schema:
    *               type: object
    *               properties:
    *                 imageId:
    *                   type: string
    *                   description: The image id
    *               example:
    *                 imageId: "e49bb982bd2e419682565e285a899a2f"
    */
    router.post('/add', createMulterMiddleware(), addImageHandler);

    /**
    * @swagger
    * /images/{id}:
    *   get:
    *     summary: Get image by id
    *     tags: [Images]
    *     parameters:
    *       - in: path
    *         name: id
    *         schema:
    *           type: string
    *         required: true
    *         description: The image id
    *     responses:
    *       200:
    *         description: The image
    *         content:
    *           image/*:
    *             schema:
    *               type: string
    *               format: binary
    *       404:
    *         description: Image not found
    */
    router.get('/:id', getImageHandler(getImageById));

    return router;
}
