import { google } from "@google-cloud/language/build/protos/protos"

export type StringInfo = {
    entities: google.cloud.language.v1.IEntity[],
    numOfWords: number
}

export type TicketInfo = {
    id: number,
    userId: number,
    eventName: string,
    categoryName: string,
    description: string
}

export type AnalyzedTicketInfo = {
    id: number,
    userId: number,
    eventName: StringInfo,
    categoryName: string,
    description: StringInfo
}

export type UserInfo = {
    id: number,
    interests: string
}

export type AnalyzedUserInfo = {
    id: number,
    interests: StringInfo
}