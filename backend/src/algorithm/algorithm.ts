import { TicketUserScore, upsertScores } from "../db/scores";
import { getTicketsForAlgorithm } from "../db/tickets";
import { getUsersForAlgorithm } from "../db/users";
import { AnalyzedTicketInfo, AnalyzedUserInfo, StringInfo, TicketInfo, UserInfo } from "./types";
import { nlpClient } from "./nlpClient";
import { google } from "@google-cloud/language/build/protos/protos";
import { categoriesWords } from "./relatedWords";

const calcEntityScoreContribution = (entity: google.cloud.language.v1.IEntity, ticketCategory: string, ticketName: StringInfo, ticketDescription: StringInfo): number => {
    const categoryMatchScore = categoriesWords[ticketCategory]
        .filter(w => w.score > 1)
        .filter(word => entity.name.split(" ").some(nameWord => word.word.toLowerCase().includes(nameWord.toLowerCase())))
        .reduce((acc, curr) => acc + Math.sqrt(curr.score), 0);

    const ticketNameMatchScore =
        ticketName.entities.filter(e => e.name.toLowerCase() === entity.name.toLowerCase()).reduce((acc, curr) => acc + curr.salience, 0);

    const ticketDescriptionMatchScore =
        ticketDescription.entities.filter(e => e.name.toLowerCase() === entity.name.toLowerCase()).reduce((acc, curr) => acc + Math.sqrt(curr.salience), 0);

    return (categoryMatchScore / 100 + ticketNameMatchScore + ticketDescriptionMatchScore) * entity.salience;
}

export const getScore = (userInterests: StringInfo, ticketCategory: string, ticketName: StringInfo, ticketDescription: StringInfo): number => {
    return userInterests.entities.reduce((acc, currEntity) => acc + calcEntityScoreContribution(currEntity, ticketCategory, ticketName, ticketDescription), 0);
}

export const getStringInfo = async (str: string): Promise<StringInfo> => {
    const document: google.cloud.language.v1.IDocument = {
        content: str,
        type: "PLAIN_TEXT",
        language: "en"
    };

    const [{ entities }] = await nlpClient.analyzeEntities({ document });
    const numOfWords = str.split(" ").length;

    return { entities, numOfWords };
}

const analyzeTicket = async (ticketInfo: TicketInfo): Promise<AnalyzedTicketInfo> => ({
    id: ticketInfo.id,
    userId: ticketInfo.userId,
    categoryName: ticketInfo.categoryName,
    eventName: await getStringInfo(ticketInfo.eventName),
    description: await getStringInfo(ticketInfo.description)
});

const analyzeUser = async (userInfo: UserInfo): Promise<AnalyzedUserInfo> => ({
    id: userInfo.id,
    interests: await getStringInfo(userInfo.interests)
});

export const updateScoresForTicketsAndUsers = async (ticketIds?: number[], userIds?: number[]) => {
    const tickets: AnalyzedTicketInfo[] = await Promise.all((await getTicketsForAlgorithm(ticketIds)).map(analyzeTicket));
    const users: AnalyzedUserInfo[] = await Promise.all((await getUsersForAlgorithm(userIds)).map(analyzeUser));

    const scores: TicketUserScore[] = users.flatMap(userInfo => tickets.map(ticketInfo => ({
        userId: userInfo.id,
        ticketId: ticketInfo.id,
        score: getScore(userInfo.interests, ticketInfo.categoryName, ticketInfo.eventName, ticketInfo.description)
    })));

    await upsertScores(scores);
}