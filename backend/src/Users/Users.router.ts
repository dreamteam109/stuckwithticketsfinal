import { Router } from "express"
import { updateScoresForTicketsAndUsers } from "../algorithm/algorithm";
import { authMiddleware, refreshTokenMiddleware } from "../auth/auth";
import { editUserDetails, getContactInfoById } from "../db/users";
import { contactInfoHandler, editUserDetailsHandler, getMyUserDetailsHandler, loginHandler, logoutHandler, refreshTokenHandler, registerHandler } from "./Users.handlers";

/**
* @swagger
* tags:
*   name: Users
*   description: The Users API
*/

/**
* @swagger
* components:
*   securitySchemes:
*     accessToken:
*       type: http
*       scheme: bearer
*       bearerFormat: JWT
*     refreshToken:
*       type: http
*       scheme: bearer
*       bearerFormat: JWT
*/

/**
* @swagger
* components:
*   responses:
*     UnauthorizedError:
*       description: Access token is missing or invalid
*/

/**
* @swagger
* components:
*   schemas:
*     RegisterRequestBody:
*       type: object
*       properties:
*         email:
*           type: string
*           description: The user's email
*         password:
*           type: string
*           description: The user's passowrd
*         firstName:
*           type: string
*           description: The user's first name
*         lastName:
*           type: string
*           description: The user's last name
*         phoneNumber:
*           type: string
*           description: The user's phone number
*         imageId:
*           type: string
*           nullable: true
*           description: The user's image id
*         interests:
*           type: string
*           nullable: false
*           description: The user's interests
*       example:
*         email: "IsraelIsraeli@gmail.com"
*         password: "123456"
*         firstName: "Israel"
*         lastName: "Israeli"
*         phoneNumber: "050-1234567"
*         imageId: "e49bb982bd2e419682565e285a899a2f"
*         interests: "I like rock music"
*     LoginRequestBody:
*       type: object
*       properties:
*         email:
*           type: string
*           description: The user's email
*         password:
*           type: string
*           description: The user's password
*       example:
*         email: "IsraelIsraeli@gmail.com"
*         password: "123456"
*     AccessAndRefreshTokens:
*       type: object
*       properties:
*         accessToken:
*           type: string
*           description: The access token
*         refreshToken:
*           type: string
*           description: The refresh token
*       example:
*         accessToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJlbWFpbCI6Iml0YXk2NDdAZ21haWwuY29tIn0sImlhdCI6MTY1MzA2NjkxNywiZXhwIjoxNjUzMDY3MjE3fQ.d-lPAX_2IX4YqDFA2wtiKd3WGHSYWA6Kt5U-thyg-nc"
*         refreshToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJlbWFpbCI6Iml0YXk2NDdAZ21haWwuY29tIn0sImlhdCI6MTY1MzA2NjkxN30.HHhUnt3dPVLSlgl9st92iZ6ClZfo3mQ3lVc0kSN1m5Q"
*     EditUserRequestBody:
*       type: object
*       properties:
*         firstName:
*           type: string
*           description: The user's first name
*         lastName:
*           type: string
*           description: The user's last name
*         phoneNumber:
*           type: string
*           description: The user's phone number
*         imageId:
*           type: string
*           nullable: true
*           description: The user's image id
*         interests:
*           type: string
*           nullable: false
*           description: The user's interests
*       example:
*         firstName: "Israel"
*         lastName: "Israeli"
*         phoneNumber: "050-1234567"
*         imageId: "e49bb982bd2e419682565e285a899a2f"
*         interests: "I like rock music"
*     User:
*       type: object
*       properties:
*         id:
*           type: number
*           description: The user's id
*         firstName:
*           type: string
*           description: The user's first name
*         lastName:
*           type: string
*           description: The user's last name
*         email:
*           type: string
*           description: The user's email
*         password:
*           type: string
*           description: The user's passowrd
*         phoneNumber:
*           type: string
*           description: The user's phone number
*         imageId:
*           type: string
*           nullable: true
*           description: The user's image id
*         interests:
*           type: string
*           nullable: false
*           description: The user's interests
*       example:
*         id: 1
*         firstName: "Israel"
*         lastName: "Israeli"
*         email: "IsraelIsraeli@gmail.com"
*         password: "123456"
*         phoneNumber: "050-1234567"
*         imageId: "e49bb982bd2e419682565e285a899a2f"
*         interests: "I like rock music"
*     ContactInfo:
*       type: object
*       properties:
*         id:
*           type: number
*           description: The user's id
*         firstName:
*           type: string
*           description: The user's first name
*         lastName:
*           type: string
*           description: The user's last name
*         email:
*           type: string
*           description: The user's email
*         phoneNumber:
*           type: string
*           description: The user's phone number
*         imageId:
*           type: string
*           nullable: true
*           description: The user's image id
*       example:
*         id: 1
*         firstName: "Israel"
*         lastName: "Israeli"
*         email: "IsraelIsraeli@gmail.com"
*         phoneNumber: "050-1234567"
*         imageId: "e49bb982bd2e419682565e285a899a2f"
*/

export const createUsersRouter = () => {
    const router = Router();

    /**
    * @swagger
    * /users/register:
    *   post:
    *     summary: Register a new user
    *     tags: [Users]
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/RegisterRequestBody'
    *     responses:
    *       201:
    *         description: Register successful
    *       400:
    *         description: Invalid or missing registration data
    *       409:
    *         description: The given email is already registered
    */
    router.post('/register', registerHandler);

    /**
    * @swagger
    * /users/login:
    *   post:
    *     summary: Login with email and password
    *     tags: [Users]
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/LoginRequestBody'
    *     responses:
    *       200:
    *         description: Login successful
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/AccessAndRefreshTokens'
    *       400:
    *         description: Invalid or missing email or password
    *       401:
    *         description: Wrong username or password
    */
    router.post('/login', loginHandler);

    /**
    * @swagger
    * /users/refreshToken:
    *   post:
    *     summary: Request a new access and refresh tokens
    *     tags: [Users]
    *     security:
    *       - refreshToken: []
    *     responses:
    *       200:
    *         description: Created new tokens successfully
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/AccessAndRefreshTokens'
    *       401:
    *         description: Refresh token is missing or invalid
    *       403:
    *         description: Invalid request
    */
    router.post('/refreshToken', refreshTokenMiddleware, refreshTokenHandler);

    /**
    * @swagger
    * /users/logout:
    *   post:
    *     summary: Logout
    *     tags: [Users]
    *     security:
    *       - refreshToken: []
    *     responses:
    *       200:
    *         description: Logout successful
    *       401:
    *         description: Refresh token is missing or invalid
    *       403:
    *         description: Invalid request
    */
    router.post('/logout', refreshTokenMiddleware, logoutHandler);

    /**
    * @swagger
    * /users/edit:
    *   post:
    *     summary: Edit my user info
    *     tags: [Users]
    *     security:
    *       - accessToken: []
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/EditUserRequestBody'
    *     responses:
    *       200:
    *         description: The edit completed successfully
    *       400:
    *         description: Invalid or missing user data on edit
    *       401:
    *         $ref: "#/components/responses/UnauthorizedError"
    */
    router.post('/edit', authMiddleware, editUserDetailsHandler(editUserDetails, updateScoresForTicketsAndUsers));

    /**
    * @swagger
    * /users/me:
    *   get:
    *     summary: Get my user info
    *     tags: [Users]
    *     security:
    *       - accessToken: []
    *     responses:
    *       200:
    *         description: The user info
    *         content:
    *           application/json:
    *             schema:
    *               $ref: "#/components/schemas/User"
    *       401:
    *         $ref: "#/components/responses/UnauthorizedError"
    */
    router.get('/me', authMiddleware, getMyUserDetailsHandler);

    /**
    * @swagger
    * /users/contactInfo/{id}:
    *   get:
    *     summary: Get the contact info by user id
    *     tags: [Users]
    *     parameters:
    *       - in: path
    *         name: id
    *         schema:
    *           type: number
    *         required: true
    *         description: The user id
    *     responses:
    *       200:
    *         description: The contact info
    *         content:
    *           application/json:
    *             schema:
    *               $ref: "#/components/schemas/ContactInfo"
    */
    router.get('/contactInfo/:id', contactInfoHandler(getContactInfoById));

    return router;
}
