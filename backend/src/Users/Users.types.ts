import { CommonColumns } from "../db/types";
import { Email, HashedPassword, NonEmptyString, Password, PhoneNumber } from "../types";

export interface User extends CommonColumns {
    firstName: NonEmptyString;
    lastName: NonEmptyString;
    email: Email;
    password: HashedPassword;
    phoneNumber: string;
    imageId?: string;
    interests: string;
}

export interface LoginRequestBody {
    email: Email;
    password: Password;
}

export interface EditUserDetailsRequestBody {
    firstName: NonEmptyString;
    lastName: NonEmptyString;
    phoneNumber: PhoneNumber;
    imageId?: string;
    interests: string;
}

export type UserForCreate = Omit<User, "id" | "isDeleted" | "lastUpdateDate" | "creationDate">;

export type UserForEdit = Omit<User, "isDeleted" | "lastUpdateDate" | "creationDate" | "email" | "password">;

export interface RegisterRequestBody extends Omit<UserForCreate, "password"> {
    password: Password;
}
