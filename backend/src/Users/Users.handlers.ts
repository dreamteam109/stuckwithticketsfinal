import { Request, Response, NextFunction } from "express";
import { isEditUserDetailsRequestBody, isLoginRequestBody, isRegisterRequestBody, isUser } from "./Users.typeguards";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import passport from "passport";
import { ContactInfo, createUser, getUserByEmailForAuth } from "../db/users";
import { UserForEdit } from "./Users.types";
import { deleteRefreshToken, deleteRefreshTokensOfUser, getRefreshTokensOfUser, insertRefreshToken, replaceRefreshToken } from "../db/refreshTokens";
import { updateScoresForTicketsAndUsers } from "../algorithm/algorithm";

export const registerHandler = async (req: Request, res: Response, next: NextFunction) => {
    if (isRegisterRequestBody(req.body)) {
        const { email, password, firstName, lastName, phoneNumber, imageId, interests } = req.body;

        try {
            const existingUserWithEmail = await getUserByEmailForAuth(email);

            if (existingUserWithEmail) {
                return res.status(409).send("The given email is already registered");
            }

            const hashedPassword = await bcrypt.hash(password, 10);

            const userId = await createUser({
                email,
                password: hashedPassword,
                firstName,
                lastName,
                phoneNumber,
                imageId,
                interests
            });

            await updateScoresForTicketsAndUsers(undefined, [userId]);

            res.sendStatus(201);
        } catch (error) {
            next(error);
        }

    } else {
        res.status(400).send("invalid or missing registration data");
    }
}

export const loginHandler = async (req: Request, res: Response, next: NextFunction) => {
    if (isLoginRequestBody(req.body)) {
        passport.authenticate(
            'login',
            { session: false },
            async (error, user, info) => {
                try {
                    if (error) {
                        return next(error);
                    }

                    if (!user) {
                        return res.status(401).send(info.message);
                    }

                    req.login(
                        user,
                        { session: false },
                        async (error) => {
                            if (error) return next(error);

                            const body = { id: user.id, email: user.email };
                            const accessToken = jwt.sign(
                                { user: body },
                                process.env.JWT_SECRET,
                                { expiresIn: process.env.JWT_EXPIRATION }
                            );
                            const refreshToken = jwt.sign(
                                { user: body },
                                process.env.JWT_REFRESH_SECRET
                            );

                            await insertRefreshToken(user.id, refreshToken);

                            return res.json({
                                accessToken: accessToken,
                                refreshToken: refreshToken
                            });
                        }
                    );
                } catch (error) {
                    return next(error);
                }
            }
        )(req, res, next);
    } else {
        res.status(400).send("invalid or missing email or password");
    }
}

export const refreshTokenHandler = async (req: Request, res: Response, next: NextFunction) => {
    if (isUser(req.user)) {
        const authHeaders = req.headers['authorization'];
        const refreshToken = authHeaders && authHeaders.split(" ")[1];
        // console.log({refreshToken});

        let userRefreshTokens = await getRefreshTokensOfUser(req.user.id);
        // console.log({userRefreshTokens});

        if (!userRefreshTokens.includes(refreshToken)) {
            // Invalidate all user tokens
            await deleteRefreshTokensOfUser(req.user.id);
            return res.status(403).send("invalid request");
        }

        const body = { id: req.user.id, email: req.user.email };
        const accessToken = jwt.sign(
            { user: body },
            process.env.JWT_SECRET,
            { expiresIn: process.env.JWT_EXPIRATION }
        );
        const newRefreshToken = jwt.sign(
            { user: body },
            process.env.JWT_REFRESH_SECRET
        );

        await replaceRefreshToken(req.user.id, refreshToken, newRefreshToken);

        return res.json({
            accessToken: accessToken,
            refreshToken: newRefreshToken
        });
    } else {
        res.status(403).send("invalid request");
    }
}

export const logoutHandler = async (req: Request, res: Response, next: NextFunction) => {
    if (isUser(req.user)) {
        const authHeaders = req.headers['authorization'];
        const refreshToken = authHeaders && authHeaders.split(" ")[1];

        let userRefreshTokens = await getRefreshTokensOfUser(req.user.id);

        if (!userRefreshTokens.includes(refreshToken)) {
            // Invalidate all user tokens
            await deleteRefreshTokensOfUser(req.user.id);
            return res.status(403).send("invalid request");
        }

        await deleteRefreshToken(req.user.id, refreshToken);

        res.sendStatus(200);
    } else {
        res.status(403).send("invalid request");
    }
}

export const editUserDetailsHandler = (
    editUserDetails: (user: UserForEdit) => Promise<void>,
    updateScoresForTicketsAndUsers: (ticketIds?: number[], userIds?: number[]) => Promise<void>
) => async (req: Request, res: Response) => {
    if (isEditUserDetailsRequestBody(req.body) && isUser(req.user)) {
        const { firstName, lastName, phoneNumber, imageId, interests } = req.body;
        const userForEdit: UserForEdit = {
            id: req.user.id,
            firstName,
            lastName,
            phoneNumber,
            imageId,
            interests
        };
        await editUserDetails(userForEdit);
        await updateScoresForTicketsAndUsers(undefined, [req.user.id]);

        res.send();
    } else {
        res.status(400).send("invalid or missing user data on edit");
    }
}

export const getMyUserDetailsHandler = async (req: Request, res: Response) => {
    if (isUser(req.user)) {
        res.send(req.user)
    } else {
        res.status(400).send("invalid or missing user data");
    }
}

export const contactInfoHandler = (getContactInfoById: (id: number) => Promise<ContactInfo>) => async (req: Request, res: Response) => {
    // TODO: maybe validate...
    const userId = Number(req.params.id);
    const contactInfo = await getContactInfoById(userId);
    res.json(contactInfo);
}
