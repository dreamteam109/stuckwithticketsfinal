import { UserInfo } from "../algorithm/types";
import { User, UserForCreate, UserForEdit } from "../Users/Users.types";
import pool from "./pool";
import { isNotNil } from "./tickets";

export const getUsersForAlgorithm = async (userIds?: number[]): Promise<UserInfo[]> => {

  const userIdsSearch = `and u.id = ANY($1::int[])`;

  const { rows } = await pool.query(
    ` select id, interests
      from stuckwithtickets.users u
      where is_deleted = false
      ${isNotNil(userIds) ? userIdsSearch : ""}
    `,
    userIds ? [userIds] : []
  );

  return rows;
};

export const createUser = async (user: UserForCreate): Promise<number> => {
  const { email, password, firstName, lastName, phoneNumber, imageId, interests } = user;

  const { rows } = await pool.query(`
    insert into stuckwithtickets.users (first_name, last_name, email, password, phone_number, image_id, interests)
    values ($1, $2, $3, $4, $5, $6, $7) returning id
  `, [firstName, lastName, email, password, phoneNumber, imageId, interests]
  );

  return rows[0].id;
};

export const editUserDetails = async (user: UserForEdit): Promise<void> => {
  const { id, firstName, lastName, phoneNumber, imageId, interests } = user;

  await pool.query(
    ` update stuckwithtickets.users 
     set first_name = $2,
         last_name = $3,
         phone_number = $4,
         image_id = $5,
         interests = $6,
         last_update_date = now()
        where id = $1 and is_deleted = false
 `
    , [id, firstName, lastName, phoneNumber, imageId, interests]);
}

export type UserInfoForAuth = Omit<User, "isDeleted" | "lastUpdateDate" | "creationDate">;

export const getUserByEmailForAuth = async (email: string): Promise<UserInfoForAuth> => {
  const { rows }: { rows: UserInfoForAuth[] } = await pool.query(`
    select
        id,
        first_name as "firstName",
        last_name as "lastName",
        email,
        password,
        phone_number as "phoneNumber",
        image_id as "imageId",
        interests
    from stuckwithtickets.users
    where is_deleted = false
        and email = $1
  `, [email]
  );
  return rows[0];
};

export type ContactInfo = Omit<User, "password" | "interests" | "isDeleted" | "lastUpdateDate" | "creationDate">;

export const getContactInfoById = async (userId: number): Promise<ContactInfo> => {
  const { rows }: { rows: ContactInfo[] } = await pool.query(`
    select
      id,
      first_name as "firstName",
      last_name as "lastName",
      email,
      phone_number as "phoneNumber",
      image_id as "imageId"
    from stuckwithtickets.users
    where is_deleted = false
      and id = $1
  `, [userId]
  );
  return rows[0];
}
