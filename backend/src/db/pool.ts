import { Pool, PoolConfig } from "pg";

const config : PoolConfig = {
    application_name: "StuckWithTickets Backend"
};

const pool = new Pool(config);

export default pool;
