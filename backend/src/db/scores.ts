import pool from "./pool";
import { uniq } from 'ramda';

export type TicketUserScore = {
    ticketId: number,
    userId: number,
    score: number
}

export const upsertScores = async (scores: TicketUserScore[]) => {
    await pool.query(
        `
        delete from stuckwithtickets.users_and_tickets_scores
        where (user_id, ticket_id) in (${uniq(scores.map(score => `(${score.userId}, ${score.ticketId})`)).join(", ")})
        `
    );

    await pool.query(
        `
        insert into stuckwithtickets.users_and_tickets_scores
        values ${scores.map(score => `(${score.userId}, ${score.ticketId}, ${score.score})`).join(", ")}
        `
    );

    await pool.query(
        `
        update stuckwithtickets.tickets t
        set last_update_date = now()
        where t.id = ANY($1::int[])
        `,
        [uniq(scores.map(score => score.ticketId))]
    );
}

export const removeTicketFromScores = async (ticketId: number) => {
    await pool.query(
        `
        delete from stuckwithtickets.users_and_tickets_scores
        where ticket_id = $1
        `,
        [ticketId]
    );
}

export const removeUserFromScores = async (userId: number) => {
    await pool.query(
        `
        delete from stuckwithtickets.users_and_tickets_scores
        where user_id = $1
        `,
        [userId]
    );
}