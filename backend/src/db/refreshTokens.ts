import pool from "./pool";

export const insertRefreshToken = async (userId: number, refreshToken: string): Promise<void> => {
    await pool.query(`
        insert into stuckwithtickets.refresh_tokens (user_id, refresh_token)
        values ($1, $2)
    `, [userId, refreshToken]
    );
};

export const deleteRefreshToken = async (userId: number, refreshToken: string): Promise<void> => {
    await pool.query(`
        delete from stuckwithtickets.refresh_tokens
        where user_id = $1
        and refresh_token = $2
    `, [userId, refreshToken]);
};

export const replaceRefreshToken = async (userId: number, oldRefreshToken: string, newRefreshToken: string): Promise<void> => {
    await pool.query(`
        update stuckwithtickets.refresh_tokens
        set refresh_token = $3
        where user_id = $1
        and refresh_token = $2
    `, [userId, oldRefreshToken, newRefreshToken]);
};

export const getRefreshTokensOfUser = async (userId: number): Promise<string[]> => {
    const { rows } = await pool.query(`
        select refresh_token
        from stuckwithtickets.refresh_tokens
        where user_id = $1
    `, [userId]);

    return rows.map(row => row.refresh_token);
}

export const deleteRefreshTokensOfUser = async (userId: number): Promise<void> => {
    await pool.query(`
        delete from stuckwithtickets.refresh_tokens
        where user_id = $1
    `, [userId]);
}
