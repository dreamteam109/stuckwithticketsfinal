
export const initSwagger = (port, app) => {

if (process.env.NODE_ENV == "development") {
    const swaggerUI = require("swagger-ui-express");
    const swaggerJsDoc = require("swagger-jsdoc");
    const options = {
        definition: {
            openapi: "3.0.0",
            info: {
                title: "Stuck With Tickets API",
                version: "1.0.0",
                description: "Stuck With Tickets application API",
            },
            servers: [{ url: "http://localhost:" + port }],
        },
        apis: ["**/*.router.ts"],
    };
    const specs = swaggerJsDoc(options);
    app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));
}
}