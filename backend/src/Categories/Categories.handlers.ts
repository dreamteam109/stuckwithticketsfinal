import { Request, Response } from "express";

export const getCategoriesHandler = (getAllCategories: any) => async (req: Request, res: Response) => {
    const categories = await getAllCategories();
    res.json(categories);
}
