import { Router } from "express";
import { getAllCategories } from "../db/categories";
import { getCategoriesHandler } from "./Categories.handlers";

/**
* @swagger
* components:
*   schemas:
*     Category:
*       type: object
*       properties:
*         id:
*           type: number
*           description: The Category's id
*         name:
*           type: string
*           description: The Category's description 
*       example:
*         id: 1
*         name: "Music"
*/
export const createCategoriesRouter = () => {
    const router = Router();

    /**
    * @swagger
    * /categories:
    *   get:
    *     summary: Get my categories
    *     tags: [Categories]
    *     responses:
    *       200:
    *         description: The categories list
    *         content:
    *           application/json:
    *             schema:
    *               type: array
    *               items:
    *                 $ref: "#/components/schemas/Category"
    */
    router.get('/', getCategoriesHandler(getAllCategories));

    return router;
}
