import { Request, Response } from "express";
import { isUser } from "../Users/Users.typeguards";
import { isEditTicketRequestBody, isSearchTicketsRequestBody, isSellTicketRequestBody } from "./Tickets.typeguards";

export const sellTicketHandler = (
    sellTicket: (id, title, category, description, eventTimestamp, price, amount, imageId, x, y) => Promise<number>,
    updateScoresForTicketsAndUsers: (ticketIds?: number[], userIds?: number[]) => Promise<void>
) => async (req: Request, res: Response) => {
    if (isSellTicketRequestBody(req.body) && isUser(req.user)) {
        const { amount, category, description, eventTimestamp, price, title, imageId, x, y } = req.body;
        const ticketId = await sellTicket(req.user.id, title, category, description, eventTimestamp, price, amount, imageId, x, y);
        await updateScoresForTicketsAndUsers([ticketId]);
        res.send();
    } else {
        res.status(400).send("invalid or missing ticket data");
    }
}

export const editTicketHandler = (
    editTicket: (ticketId, title, categoryId, description, eventTimestamp, price, amount, imageId, x, y) => Promise<void>,
    updateScoresForTicketsAndUsers: (ticketIds?: number[], userIds?: number[]) => Promise<void>
) => async (req: Request, res: Response) => {
    if (isEditTicketRequestBody(req.body)) {
        const { title, category, description, eventTimestamp, price, amount, imageId, x, y } = req.body;
        const ticketId = Number(req.params.id);

        await editTicket(ticketId, title, category, description, eventTimestamp, price, amount, imageId, x, y);
        await updateScoresForTicketsAndUsers([ticketId]);
        res.send();
    } else {
        res.status(400).send("invalid or missing ticket data");
    }
}

export const deleteTicketHandler = (getTicketById: any, logicalDeleteTicket: any, removeTicketFromScores: any) => async (req: Request, res: Response) => {
    if (isUser(req.user)) {
        const ticketId = Number(req.params.id);
        const ticket = await getTicketById(ticketId);

        if (!ticket || ticket.userId !== req.user.id) {
            return res.sendStatus(400);
        } else {
            await logicalDeleteTicket(ticketId);
            await removeTicketFromScores(ticketId);
            return res.send();
        }
    } else {
        res.status(400).send("invalid or missing user data");
    }
}

export const searchTicketsHandler = (searchTickets: any) => async (req: Request, res: Response) => {
    if (isSearchTicketsRequestBody(req.body)) {
        const userId = isUser(req.user) ? req.user.id: undefined;
        const { lastUpdateTimestamp, categories, endTimestamp, priceLimit, startTimestamp, text, amount } = req.body;
        const tickets = await searchTickets(lastUpdateTimestamp, text, categories, priceLimit, amount, startTimestamp, endTimestamp, userId);
        res.json(tickets);
    } else {
        res.status(400).send("invalid or missing search data");
    }
}

export const getMyTicketsHandler = (getTicketsByUserId: any) => async (req: Request, res: Response) => {
    if (isUser(req.user)) {
        const tickets = await getTicketsByUserId(req.user.id);
        res.json(tickets);
    } else {
        res.status(400).send("invalid or missing user data");
    }
}

export const getTicketDetails = (getTicketById: any) => async (req: Request, res: Response) => {
    const ticket = await getTicketById(Number(req.params.id));

    if (ticket) {
        res.json(ticket);
    } else {
        res.status(404).send("ticket not found");
    }
}
