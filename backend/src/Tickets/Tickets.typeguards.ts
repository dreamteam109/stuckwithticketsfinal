import { isNonEmptyString, isPositiveNumber, isNumber } from "../typeguards";
import {
  EditTicketRequestBody,
  SearchTicketsRequestBody,
  SellTicketRequestBody,
} from "./Tickets.types";

export const isSellTicketRequestBody = (
  body: any
): body is SellTicketRequestBody => {

return isPositiveNumber(body?.category) &&
    isNonEmptyString(body?.title) &&
    isNonEmptyString(body?.description) && 
    isNumber(body?.eventTimestamp) &&
    isPositiveNumber(body?.price) &&
    isPositiveNumber(body?.amount) && 
    isNumber(body.x) &&
    isNumber(body.y);

};

export const isEditTicketRequestBody = (
  body: any
): body is EditTicketRequestBody => isSellTicketRequestBody(body);

export const isSearchTicketsRequestBody = (
  body: any
): body is SearchTicketsRequestBody => true;
