import { Router } from "express"
import { updateScoresForTicketsAndUsers } from "../algorithm/algorithm";
import { authMiddleware, authMiddlewareOptionalUser } from "../auth/auth";
import { removeTicketFromScores } from "../db/scores";
import { getTicketById, logicalDeleteTicket, getTicketsByUserId, editTicket, sellTicket, searchTickets, } from "../db/tickets";
import { deleteTicketHandler, editTicketHandler, getMyTicketsHandler, getTicketDetails, searchTicketsHandler, sellTicketHandler } from "./Tickets.handlers";

/**
* @swagger
* components:
*   schemas:
*     SellTicketRequestBody:
*       type: object
*       properties:
*         title:
*           type: string
*           nullable: false
*           description: The ticket's title
*         category:
*           type: number
*           description: The ticket's category
*         description:
*           type: string
*           description: The ticket's description
*         eventTimestamp:
*           type: number
*           description: The ticket's event time
*         price:
*           type: number
*           description: The ticket's price
*         amount:
*           type: number
*           description: The ticket's amount
*         imageId:
*           type: string
*           nullable: true
*           description: The ticket's image id
*         x:
*           type: number
*           description: The ticket's x coordinate
*         y:
*           type: number
*           description: The ticket's y coordinate
*       example:
*         title: "Yoni Bloch"
*         category: 1
*         description: "A Rock musican show"
*         eventTimestamp: 1519211809934
*         price: 170
*         amount: 2
*         imageId: "e49bb982bd2e419682565e285a899a2f"
*         x: 31.66
*         y: 35.03
*     EditTicketRequestBody:
*       type: object
*       properties:
*         id:
*           type: number
*           description: The ticket's id
*         title:
*           type: string
*           nullable: false
*           description: The ticket's title
*         category:
*           type: number
*           description: The ticket's category
*         description:
*           type: string
*           description: The ticket's description
*         eventTimestamp:
*           type: number
*           description: The ticket's event time
*         price:
*           type: number
*           description: The ticket's price
*         amount:
*           type: number
*           description: The ticket's amount
*         imageId:
*           type: string
*           nullable: true
*           description: The ticket's image id
*         x:
*           type: number
*           description: The ticket's x coordinate
*         y:
*           type: number
*           description: The ticket's y coordinate
*       example:
*         id: 1
*         title: "Yoni Bloch"
*         category: 1
*         description: "A Rock musican show"
*         eventTimestamp: 1519211809934
*         price: 170
*         amount: 2
*         imageId: "e49bb982bd2e419682565e285a899a2f"
*         x: 31.66
*         y: 35.03
*     SearchTicketsRequestBody:
*       type: object
*       properties:
*         lastUpdateTimestamp: 
*           type: number
*           nullable: true
*           description: The ticket's last update time
*         text: 
*           type: string
*           nullable: true
*           description: The ticket's text
*         startTimestamp: 
*           type: number
*           nullable: true
*           description: The ticket's start time
*         endTimestamp: 
*           type: number
*           nullable: true
*           description: The ticket's end time
*         categories: 
*           type: array
*           nullable: true
*           description: The list of categories
*           items:
*              type: number
*              description: The category's id
*         priceLimit: 
*           type: number
*           nullable: true
*           description: The ticket's price limit
*         amount: 
*           type: number
*           nullable: true
*           description: The ticket's amount
*       example:
*         lastUpdateTimestamp: 1519211809934
*         text: "A Rock musican show"
*         startTimestamp: 1519211809935
*         endTimestamp: 1519211809936
*         category: [1,2]
*         price: 170
*         amount: 2
*     Ticket:
*       type: object
*       properties:
*         id:
*           type: number
*           description: The ticket's id
*         userId:
*           type: number
*           description: The ticket's user id
*         eventName:
*           type: string
*           description: The ticket's title
*         categoryId:
*           type: number
*           description: The ticket's category id
*         categoryName:
*           type: string
*           description: The ticket's category name
*         description:
*           type: string
*           description: The ticket's description
*         eventDatetime:
*           type: string
*           description: The ticket's event date & time
*         price:
*           type: number
*           description: The ticket's price
*         amount:
*           type: number
*           description: The ticket's amount
*         imageId:
*           type: string
*           nullable: true
*           description: The ticket's image id
*         x:
*           type: number
*           description: The ticket's x coordinate
*         y:
*           type: number
*           description: The ticket's y coordinate
*         isDeleted:
*           type: boolean
*           description: Whether or not the ticket was (logically) deleted
*         lastUpdateDate:
*           type: string
*           description: The ticket's last update date & time
*       example:
*         id: 1
*         userId: 1
*         eventName: "Yoni Bloch"
*         categoryId: 1
*         categoryName: "Music"
*         description: "A Rock musican show"
*         eventDatetime: "2022-05-04T09:11:58.702Z"
*         price: 170
*         amount: 2
*         imageId: "e49bb982bd2e419682565e285a899a2f"
*         x: 31.66
*         y: 35.03
*         isDeleted: false
*         lastUpdateDate: "2022-04-07T09:11:58.702Z"
*/

export const createTicketsRouter = () => {
    const router = Router();

    /**
    * @swagger
    * /tickets/sell:
    *   post:
    *     summary: sell a ticket
    *     tags: [Tickets]
    *     security:
    *       - accessToken: []
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/SellTicketRequestBody'
    *     responses:
    *       200:
    *         description: The ticket added for selling completed successfully
    *       400:
    *         description: Invalid or missing ticket data on create
    */
    router.post('/sell', authMiddleware, sellTicketHandler(sellTicket, updateScoresForTicketsAndUsers));

    /**
    * @swagger
    * /tickets/edit:
    *   post:
    *     summary: Edit a ticket
    *     tags: [Tickets]
    *     security:
    *       - accessToken: []
    *     parameters:
    *       - in: path
    *         name: id
    *         schema:
    *           type: number
    *         required: true
    *         description: The ticket id
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/EditTicketRequestBody'
    *     responses:
    *       200:
    *         description: The edit completed successfully
    *       400:
    *         description: Invalid or missing ticket data on edit
    */
    router.post('/edit/:id', authMiddleware, editTicketHandler(editTicket, updateScoresForTicketsAndUsers));

    /**
    * @swagger
    * /tickets/delete:
    *   delete:
    *     summary: Delete a ticket
    *     tags: [Tickets]
    *     security:
    *       - accessToken: []
    *     parameters:
    *       - in: path
    *         name: id
    *         schema:
    *           type: number
    *         required: true
    *         description: The ticket id
    *     responses:
    *       200:
    *         description: The delete completed successfully
    *       400:
    *         description: Invalid or missing ticket data on delete
    */
    router.delete('/:id', authMiddleware, deleteTicketHandler(getTicketById,logicalDeleteTicket, removeTicketFromScores));

    /**
    * @swagger
    * /tickets/search:
    *   post:
    *     summary: Search a ticket
    *     tags: [Tickets]
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
*                 $ref: '#/components/schemas/SearchTicketsRequestBody'
    *     responses:
    *       200:
    *         description: The search completed successfully
    *         content:
    *           application/json:
    *             schema:
    *               type: array
    *               items:
    *                 $ref: "#/components/schemas/Ticket"
    *       400:
    *         description: Invalid or missing search data
    */
    router.post('/search', authMiddlewareOptionalUser, searchTicketsHandler(searchTickets));

    /**
    * @swagger
    * /tickets/me:
    *   get:
    *     summary: Get my tickets
    *     tags: [Tickets]
    *     security:
    *       - accessToken: []
    *     responses:
    *       200:
    *         description: The user's tickets
    *         content:
    *           application/json:
    *             schema:
    *               type: array
    *               items:
    *                 $ref: "#/components/schemas/Ticket"
    *       400:
    *         description: Invalid or missing user data on get my ticket
    */
    router.get('/me', authMiddleware, getMyTicketsHandler(getTicketsByUserId));

    /**
    * @swagger
    * /tickets/{id}:
    *   get:
    *     summary: Get the ticket details by ticket id
    *     tags: [Tickets]
    *     parameters:
    *       - in: path
    *         name: id
    *         schema:
    *           type: number
    *         required: true
    *         description: The ticket id
    *     responses:
    *       200:
    *         description: The ticket details
    *         content:
    *           application/json:
    *             schema:
    *               $ref: "#/components/schemas/Ticket"
    *       404:
    *         description: Ticket not found
    */
    router.get('/:id', getTicketDetails(getTicketById));

    return router;
}
